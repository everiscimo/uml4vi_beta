package com.br.fatecmc.case_b.impl;

import java.util.ArrayList;
import java.util.List;

import com.br.fatecmc.case_b.domain.Actor;
import com.br.fatecmc.case_b.domain.Note;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.UMLElement;
import com.br.fatecmc.case_b.domain.UseCase;
import com.br.fatecmc.case_b.interfaces.Project;

public class ProjectImpl implements Project {

	private List<Actor> actorList;
	private List<UseCase> useCaseList;
	private List<Note> noteList;
	private List<Relationship> relationshipList;
	private String name;
	private String version;
	private List<com.br.fatecmc.case_b.domain.Class> classList;
	/**
	 * @return the actorList
	 */
	public List<Actor> getActorList() {

		if(actorList==null){
			actorList = new ArrayList<Actor>();
		}
		return actorList;
	}
	
	/**
	 * @return the useCaseList
	 */
	public List<UseCase> getUseCaseList() {
		if (useCaseList == null){
			useCaseList = new ArrayList<UseCase>();
		}
		return useCaseList;
	}
	
	/**
	 * @return the noteList
	 */
	public List<Note> getNoteList() {
		if(noteList==null){
			noteList =new ArrayList<Note>();
		}
		return noteList;
	}

	/**
	 * Adiciona todas as outros relacionamento( Atores, Casos de uso e notas) e retorna em apensa uma lista 
	 * @return the relationshipList
	 */
	public List<Relationship> getRelationshipList() {
		
		relationshipList = new ArrayList<Relationship>();
		
		for(Actor actor : getActorList()){
			relationshipList.addAll(actor.getRelationshipList());
		}
		
		for(UseCase useCase : getUseCaseList()){
			relationshipList.addAll(useCase.getRelationshipList());
		}
		
		for(Note note : getNoteList()){
			relationshipList.addAll(note.getRelationshipList());
		}
		
		return relationshipList;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public Actor searchActor(String id) {
		for(Actor actor : getActorList()){
			if(actor.getId().equals(id))
				return actor;
		}
		return null;
	}

	@Override
	public UseCase searchUseCase(String id) {
		for(UseCase useCase : getUseCaseList()){
			if(useCase.getId().equals(id))
				return useCase;
		}
		return null;
	}

	@Override
	public Note searchNote(String id) {
		for(Note note : getNoteList()){
			if(note.getId().equals(id))
				return note;
		}
		return null;
	}

	@Override
	public RelationshipComponent searchComponent(String id) {
		
		RelationshipComponent component = null;
		
		component = searchActor(id);
		
		if(component == null){
			component = searchUseCase(id);
			
		}
		if(component == null){
			component = searchNote(id);

			
		}
		return component;
	}

	/**
	 * @return the classList
	 */
	public List<com.br.fatecmc.case_b.domain.Class> getClassList() {
		if(classList == null){
			classList =  new ArrayList<com.br.fatecmc.case_b.domain.Class>();
		}
		return classList;
	}

	@Override
	public UMLElement searchAll(String id) {
		UMLElement result = searchComponent(id);
		if(result==null){
			result = searchRelationship(id);
		}
		return result;
	}

	@Override
	public Relationship searchRelationship(String id) {
		for(Relationship relationship : getRelationshipList()){
			if(relationship.getId()!=null && relationship.getId().equals(id)){
				return relationship;
			}
		}
		return null;
	}
	
	
	
	
}
