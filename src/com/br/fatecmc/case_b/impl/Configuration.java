package com.br.fatecmc.case_b.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.br.fatecmc.case_b.argo_uml.parser.ArgoUMLXMIParser;
import com.br.fatecmc.case_b.argo_uml.writer.ArgoUMLXMIWriter;
import com.br.fatecmc.case_b.interfaces.XMIParser;
import com.br.fatecmc.case_b.interfaces.XMIWriter;

public class Configuration {

	private static  Configuration configuration;
	private List<XMIParser> xmiParserIterator;
	private HashMap<String, XMIWriter> mapXMIWriter;
	
	private Configuration(){
		loadXMIParsers();
		loadXMIWriters();
	}
	
	protected void loadXMIWriters(){
		mapXMIWriter = new HashMap<String, XMIWriter>();
		//codigo Temporario
		mapXMIWriter.put("argouml1.2", new ArgoUMLXMIWriter());
	
		xmiParserIterator = new ArrayList<XMIParser>();
		xmiParserIterator.add(new ArgoUMLXMIParser());
	}
	
	public XMIWriter getWriter(String toolName, String toolVersion){
		
		return mapXMIWriter.get(toolName+toolVersion).clone();
	
	}
	
	protected void loadXMIParsers(){
		xmiParserIterator = new ArrayList<XMIParser>();
		//codigo Temporario
		xmiParserIterator.add(new ArgoUMLXMIParser());
	}
	
	public List<XMIParser> getXMIParserIterator(){
		
		return xmiParserIterator;
	}
	
	public static Configuration getInstance(){
		//padrão singleton
		return  configuration == null ? new Configuration() : configuration;
	}
	
	
}
