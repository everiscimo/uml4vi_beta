package com.br.fatecmc.case_b.impl;


import java.io.File;
import java.io.FileInputStream;
import com.br.fatecmc.case_b.interfaces.Project;
import com.br.fatecmc.case_b.interfaces.XMIParser;

public abstract class XMIParserImpl implements XMIParser{

	@Override
	public Project doChain(File in) {
		Project project = null;
		try {
			
			for(XMIParser parse : Configuration.getInstance().getXMIParserIterator()){
				FileInputStream file =  new FileInputStream(in);
				if(parse.canParse(file)){
					try {
						FileInputStream fileParse =  new FileInputStream(in);;
						project = parse.parse(fileParse);
					} catch (Exception e) {
						System.err.println("Erro ao fazer o parse");
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {			
		} 	
		return project; 	
	}
}
