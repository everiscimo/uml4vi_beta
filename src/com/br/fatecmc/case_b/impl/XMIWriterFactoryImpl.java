package com.br.fatecmc.case_b.impl;


import com.br.fatecmc.case_b.interfaces.XMIWriter;
import com.br.fatecmc.case_b.interfaces.XMIWriterFactory;

public class XMIWriterFactoryImpl implements XMIWriterFactory {

	
	
	@Override
	public XMIWriter create(String toolName, String toolVersion) {
		
		return Configuration.getInstance().getWriter(toolName, toolVersion);
	}

}
