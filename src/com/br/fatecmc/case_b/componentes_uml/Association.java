package com.br.fatecmc.case_b.componentes_uml;

import java.util.ArrayList;
import java.util.List;

public class Association {
	private String id;
	private String name;
	private boolean isSpecification;
	private boolean isRoot;
	private boolean isLeaf;
	private boolean isAbstract;
	private boolean isNavegation;
	private int direction;
	
	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public int getDirection() {
		return direction;
	}
	
	public void setNavegation(boolean isNavegation) {
		this.isNavegation = isNavegation;
	}
	
	public boolean isNavegation() {
		return isNavegation;
	}
	
	private List<ElementAssociation> elementList;
	
	public Association(){
		elementList = new ArrayList<ElementAssociation>();
	}
	
	public void addElement(ElementAssociation element){
		elementList.add(element);
		if(elementList.size()!=2)
			isNavegation = false;
		else{
			if(elementList.get(0).isNavigable() == elementList.get(1).isNavigable()){
				isNavegation = false;
			}
			else{
				isNavegation = true;
				if(elementList.get(0).isNavigable())
					direction = -1;
				else
					direction = 1;
				
				
			}
		}
		
		
	}
	public void setElementList(List<ElementAssociation> elementList) {
		this.elementList = elementList;
	}
	
	public List<ElementAssociation> getElementList() {
		return elementList;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isSpecification() {
		return isSpecification;
	}
	public void setSpecification(boolean isSpecification) {
		this.isSpecification = isSpecification;
	}
	public boolean isRoot() {
		return isRoot;
	}
	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	public boolean isLeaf() {
		return isLeaf;
	}
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public boolean isAbstract() {
		return isAbstract;
	}
	public void setAbstract(boolean isAbstract) {
		this.isAbstract = isAbstract;
	}

}


