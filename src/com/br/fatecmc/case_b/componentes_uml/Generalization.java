package com.br.fatecmc.case_b.componentes_uml;

public class Generalization {
    private String id;
	private String elementChild;
    private String elementParent;

    public void setId(String id) {
		this.id = id;
	}
    
    public String getId() {
    	return id;
    }
    
    public void setElementChild(String idElemento1) {
        this.elementChild = idElemento1;
    }

    public void setElementParent(String idElemento2) {
        this.elementParent = idElemento2;
    }

    public String getElementChild() {
        return elementChild;
    }

    public String getElementParent() {
        return elementParent;
    }
}
