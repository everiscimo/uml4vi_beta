package com.br.fatecmc.case_b.componentes_uml;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EricoVeriscimo
 */
public class Extend {
	private String id;
    private String elementBase;
    private String elementExtension;

    public void setId(String id) {
		this.id = id;
	}
    
    public String getId() {
		return id;
	}
    
    public void setElementBase(String idElemento1) {
        this.elementBase = idElemento1;
    }

    public void setElementExtension(String idElemento2) {
        this.elementExtension = idElemento2;
    }

    public String getElementBase() {
        return elementBase;
    }

    public String getElementExtension() {
        return elementExtension;
    }
}
