package com.br.fatecmc.case_b.componentes_uml;

public class ElementAssociation {
	private String id;
	private String visibility;
	private boolean isSpecification;
	private boolean isNavigable;
	private String ordering;
	private String aggregation;
	private String targetScope;
	private String changeability;
	private String idMutiplicity;
	private String mutiplicityLower;
	private String mutiplicityUpper;
	private String idParticipant;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public boolean isSpecification() {
		return isSpecification;
	}
	public void setSpecification(boolean isSpecification) {
		this.isSpecification = isSpecification;
	}
	public boolean isNavigable() {
		return isNavigable;
	}
	public void setNavigable(boolean isNavigable) {
		this.isNavigable = isNavigable;
	}
	public String getOrdering() {
		return ordering;
	}
	public void setOrdering(String ordering) {
		this.ordering = ordering;
	}
	public String getAggregation() {
		return aggregation;
	}
	public void setAggregation(String aggregation) {
		this.aggregation = aggregation;
	}
	public String getTargetScope() {
		return targetScope;
	}
	public void setTargetScope(String targetScope) {
		this.targetScope = targetScope;
	}
	public String getChangeability() {
		return changeability;
	}
	public void setChangeability(String changeability) {
		this.changeability = changeability;
	}
	public String getIdMutiplicity() {
		return idMutiplicity;
	}
	public void setIdMutiplicity(String idMutiplicity) {
		this.idMutiplicity = idMutiplicity;
	}
	public String getMutiplicityLower() {
		return mutiplicityLower;
	}
	public void setMutiplicityLower(String mutiplicityLower) {
		this.mutiplicityLower = mutiplicityLower;
	}
	public String getMutiplicityUpper() {
		return mutiplicityUpper;
	}
	public void setMutiplicityUpper(String mutiplicityUpper) {
		this.mutiplicityUpper = mutiplicityUpper;
	}
	public String getIdParticipant() {
		return idParticipant;
	}
	public void setIdParticipant(String idParticipant) {
		this.idParticipant = idParticipant;
	}

}
