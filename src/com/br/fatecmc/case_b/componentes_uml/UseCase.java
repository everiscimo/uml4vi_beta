package com.br.fatecmc.case_b.componentes_uml;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EricoVeriscimo
 */
public class UseCase {
    private String id;
    private String name;
    private boolean specification;
    private boolean root;
    private boolean leaf;
    private boolean bAbstract;
    
    public UseCase(String id, String name) {
        this.id = id;
        this.name = name;
        bAbstract = leaf = root = specification = false;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean isbAbstract() {
        return bAbstract;
    }

    public void setbAbstract(boolean bAbstract) {
        this.bAbstract = bAbstract;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public boolean isRoot() {
        return root;
    }

    public void setRoot(boolean root) {
        this.root = root;
    }

    public boolean isSpecification() {
        return specification;
    }

    public void setSpecification(boolean specification) {
        this.specification = specification;
    }
    
}

