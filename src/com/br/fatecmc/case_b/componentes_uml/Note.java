package com.br.fatecmc.case_b.componentes_uml;

import java.util.ArrayList;
import java.util.List;

public class Note {
    private String id;
    private String body;
    private boolean specification;
    private List<String> idAssociations;

    public Note(){
    	idAssociations = new ArrayList<>();
    }
    public String getBody() {
        return body;
    }

    public void addIdAssociation(String id){
    	idAssociations.add(id);
    }
    public void setBody(String body) {
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getIdAssociados() {
        return idAssociations;
    }

    public void setIdAssociados(List<String> idAssociations) {
        this.idAssociations = idAssociations;
    }



    public boolean isSpecification() {
        return specification;
    }

    public void setSpecification(boolean specification) {
        this.specification = specification;
    }
}
