package com.br.fatecmc.case_b.componentes_uml;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EricoVeriscimo
 */
public class Actor {
    private String id;
    private String name;
    private boolean specification;
    private boolean root;
    private boolean leaf;
    private boolean bAbstract;

    public Actor(String id, String name) {
        this.id = id;
        this.name = name;
        bAbstract = leaf = root = specification = false;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public boolean isIsAbstract() {
        return bAbstract;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.bAbstract = isAbstract;
    }

    public boolean isIsLeaf() {
        return leaf;
    }

    public void setIsLeaf(boolean isLeaf) {
        this.leaf = isLeaf;
    }

    public boolean isIsRoot() {
        return root;
    }

    public void setIsRoot(boolean isRoot) {
        this.root = isRoot;
    }

    public boolean isIsSpecification() {
        return specification;
    }

    public void setIsSpecification(boolean isSpecification) {
        this.specification = isSpecification;
    }

    
}
