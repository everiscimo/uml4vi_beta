package com.br.fatecmc.case_b.componentes_uml;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author EricoVeriscimo
 */
public class Include {
	private String id;
    private String elementAddition;
    private String elementBase;

    public void setId(String id) {
		this.id = id;
	}
    
    public String getId() {
		return id;
	}
    
    public void setElementAddition(String idElemento1) {
        this.elementAddition = idElemento1;
    }

    public void setElementBase(String idElemento2) {
        this.elementBase = idElemento2;
    }

    public String getElementAddition() {
        return elementAddition;
    }

    public String getElementBase() {
        return elementBase;
    }
}
