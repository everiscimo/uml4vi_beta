package com.br.fatecmc.case_b.use_case;

import java.util.Date;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.br.fatecmc.case_b.componentes_uml.*;

public class CreateUseCase {
	public Document criarCasoDeUso(String nomeDoProjeto, List<Actor> atores,
			List<UseCase> casos, List<Association> associacoes,
			List<Include> includes, List<Extend> listaextend,
			List<Generalization> generalizacoes, int ContadorE) {
		int contador = ContadorE;
		String falseS = "false";
		Document doc = new Document();
		// data
		Element root = new Element("XMI");

		Namespace uml = Namespace.getNamespace("UML",
				"org.omg.xmi.namespace.UML");

		root.setAttribute("xmi.version", "1.2");
		root.addNamespaceDeclaration(uml);
		root.setAttribute("timestamp", new Date().toString());

		Element header = new Element("XMI.header");
		Element documentation = new Element("XMI.documentation");
		Element exporter = new Element("XMI.exporter");
		exporter.setText("FatecVisual");
		Element exporterVersion = new Element("XMI.exporterVersion");
		exporterVersion.setText("0.01");
		documentation.addContent(exporter);
		documentation.addContent(exporterVersion);
		header.addContent(documentation);
		Element metamodel = new Element("XMI.metamodel");
		metamodel.setAttribute("xmi.name", "UML");
		metamodel.setAttribute("xmi.version", "1.4");
		header.addContent(metamodel);

		Element content = new Element("XMI.content");
		Element Model = new Element("Model");
		Model.setNamespace(uml);
		Model.setAttribute("xmi.id", String.valueOf(0));
		Model.setAttribute("name", nomeDoProjeto);
		Model.setAttribute("isSpecification", falseS);
		Model.setAttribute("isRoot", falseS);
		Model.setAttribute("isLeaf", falseS);
		Model.setAttribute("isAbstract", falseS);
		Element ownedElement = new Element("Namespace.ownedElement");
		ownedElement.setNamespace(uml);

		// add atores
		for (Actor a : atores) {
			Element Actor = new Element("Actor");
			Actor.setNamespace(uml);
			Actor.setAttribute("xmi.id", a.getId());
			Actor.setAttribute("name", a.getName());
			Actor.setAttribute("isSpecification",
					String.valueOf(a.isIsSpecification()));
			Actor.setAttribute("isRoot", String.valueOf(a.isIsRoot()));
			Actor.setAttribute("isLeaf", String.valueOf(a.isIsLeaf()));
			Actor.setAttribute("isAbstract", String.valueOf(a.isIsAbstract()));
			ownedElement.addContent(Actor);
		}
		// add Casos de Uso
		for (UseCase c : casos) {
			Element Caso = new Element("UseCase");
			Caso.setNamespace(uml);
			Caso.setAttribute("xmi.id", c.getId());
			Caso.setAttribute("name", c.getName());
			Caso.setAttribute("isSpecification",
					String.valueOf(c.isSpecification()));
			Caso.setAttribute("isRoot", String.valueOf(c.isRoot()));
			Caso.setAttribute("isLeaf", String.valueOf(c.isLeaf()));
			Caso.setAttribute("isAbstract", String.valueOf(c.isbAbstract()));
			ownedElement.addContent(Caso);
		}
		// add associações
		for (Association ass : associacoes) {
			Element Association = new Element("Association");
			Association.setNamespace(uml);
			Association.setAttribute("xmi.id", String.valueOf(contador++));
			Association.setAttribute("name", "");
			Association.setAttribute("isSpecification", "false");
			Association.setAttribute("isRoot", "false");
			Association.setAttribute("isLeaf", "false");
			Association.setAttribute("isAbstract", "false");
			Element AssociationConnection = new Element(
					"Association.connection");
			AssociationConnection.setNamespace(uml);
			Association.addContent(AssociationConnection);
			// elemento 1
			Element AssociationEnd = new Element("AssociationEnd");
			AssociationEnd.setNamespace(uml);
			AssociationEnd.setAttribute("xmi.id", String.valueOf(contador++));
			AssociationConnection.addContent(AssociationEnd);

			Element AssociationEndparticipant = new Element(
					"AssociationEnd.participant");
			AssociationEndparticipant.setNamespace(uml);
			AssociationEnd.addContent(AssociationEndparticipant);

			Element ActorAssociacao = new Element("Actor");
			ActorAssociacao.setNamespace(uml);
			ActorAssociacao.setAttribute("xmi.idref", ass.getElementList().get(0).getIdParticipant());
			AssociationEndparticipant.addContent(ActorAssociacao);

			// elemento 2
			Element AssociationEnd2 = new Element("AssociationEnd");
			AssociationEnd2.setNamespace(uml);
			AssociationEnd2.setAttribute("xmi.id", String.valueOf(contador++));
			AssociationConnection.addContent(AssociationEnd2);

			Element AssociationEndparticipant2 = new Element(
					"AssociationEnd.participant");
			AssociationEndparticipant2.setNamespace(uml);
			AssociationEnd2.addContent(AssociationEndparticipant2);

			Element CasoAssociacao = new Element("Caso");
			CasoAssociacao.setNamespace(uml);
			CasoAssociacao.setAttribute("xmi.idref",ass.getElementList().get(1).getIdParticipant());
			AssociationEndparticipant2.addContent(CasoAssociacao);

			ownedElement.addContent(Association);
		}
		// add Extend
		for (Extend extend : listaextend) {
			Element Extend = new Element("Extend");
			Extend.setNamespace(uml);
			Extend.setAttribute("xmi.id", String.valueOf(contador++));
			Extend.setAttribute("isSpecification", "false");

			// ELemento Base
			Element base = new Element("Extend.base");
			base.setNamespace(uml);
			Element UseCaseBase = new Element("UseCase");
			UseCaseBase.setNamespace(uml);
			UseCaseBase.setAttribute("xmi.idref", extend.getElementBase());
			base.addContent(UseCaseBase);
			Extend.addContent(base);

			// ELemento extension
			Element extension = new Element("Extend.extension");
			extension.setNamespace(uml);
			Element UseCaseExtension = new Element("UseCase");
			UseCaseExtension.setNamespace(uml);
			UseCaseExtension.setAttribute("xmi.idref",
					extend.getElementExtension());
			extension.addContent(UseCaseExtension);
			Extend.addContent(extension);

			ownedElement.addContent(Extend);
		}
		// add include

		for (Include i : includes) {
			Element Include = new Element("Include");
			Include.setNamespace(uml);
			Include.setAttribute("xmi.id", String.valueOf(contador++));
			Include.setAttribute("isSpecification", "false");

			// Elemento addition
			Element addition = new Element("Include.addition");
			addition.setNamespace(uml);
			Element UseCaseAddition = new Element("UseCase");
			UseCaseAddition.setNamespace(uml);
			UseCaseAddition.setAttribute("xmi.idref", i.getElementAddition());
			addition.addContent(UseCaseAddition);
			Include.addContent(addition);

			// Elemento Base
			Element base = new Element("Include.base");
			base.setNamespace(uml);
			Element UseCaseBase = new Element("UseCase");
			UseCaseBase.setNamespace(uml);
			UseCaseBase.setAttribute("xmi.idref", i.getElementBase());
			base.addContent(UseCaseBase);
			Include.addContent(base);

			ownedElement.addContent(Include);

		}

		// add generalização

		for (Generalization g : generalizacoes) {
			Element Generalization = new Element("Generalization");
			Generalization.setNamespace(uml);
			Generalization.setAttribute("xmi.id", String.valueOf(contador++));
			Generalization.setAttribute("isSpecification", "false");

			// elemento Child
			Element child = new Element("Generalization.child");
			child.setNamespace(uml);
			Element ActorChild = new Element("Actor");
			ActorChild.setNamespace(uml);
			ActorChild.setAttribute("xmi.idref", g.getElementChild());
			child.addContent(ActorChild);
			Generalization.addContent(child);

			// elemento parent
			Element parent = new Element("Generalization.parent");
			parent.setNamespace(uml);
			Element ActorParent = new Element("Actor");
			ActorParent.setNamespace(uml);
			ActorParent.setAttribute("xmi.idref", g.getElementParent());
			parent.addContent(ActorParent);
			Generalization.addContent(parent);

			ownedElement.addContent(Generalization);
		}

		Model.addContent(ownedElement);
		content.addContent(Model);
		root.addContent(header);
		root.addContent(content);
		doc.setRootElement(root);

		return doc;
	}
}
