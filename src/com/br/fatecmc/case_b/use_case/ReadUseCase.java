package com.br.fatecmc.case_b.use_case;

import java.util.List;

import com.br.fatecmc.case_b.componentes_uml.*;

public class ReadUseCase {
	public StringBuffer leitura(List<Actor> atores, List<UseCase> casos,
			List<Association> associacoes, List<Include> includes,
			List<Extend> listaextend, List<Generalization> generalizacoes,
			List<Note> notes) {
		StringBuffer resposta = new StringBuffer();
		if (atores.size() > 0)
			resposta.append("Atores:\n");
		for (Actor a : atores) {

			resposta.append(a.getName() + "\n");
		}
		if (casos.size() > 0)
			resposta.append("Casos De uso:\n");
		for (UseCase c : casos) {

			resposta.append(c.getName() + "\n");
		}
		if (associacoes.size() > 0)
			resposta.append("Associações:\n");
		for (Association a : associacoes) {

			String name1 = "";
			String name2 = "";
			for (ElementAssociation element : a.getElementList()) {
				for (Actor at : atores) {
					if (at.getId().equals(element.getIdParticipant())) {
						if (!name1.equals(""))
							name2 = "Ator " + at.getName();
						else
							name1 = "Ator " + at.getName();
					}

				}
				for (UseCase c : casos) {
					if (c.getId().equals(element.getIdParticipant())) {
						if (!name1.equals(""))
							name2 = "Caso de Uso " + c.getName();
						else
							name1 = "Caso de Uso " + c.getName();

					}

				}
			}

			resposta.append(name1).append(" Está associado ao ").append(name2);
			if (a.isNavegation()) {
				if (a.getDirection() > 0) {
					resposta.append(" contendo direção do ").append(name1)
							.append(" para o ").append(name2);
				}
			}
			resposta.append("\n");
		}

		if (includes.size() > 0)
			resposta.append("Includes:\n");
		for (Include i : includes) {

			// System.out.println(a.getIdElemento1()+" "+a.getIdElemento2());
			String name1 = "";
			String name2 = "";

			for (UseCase c : casos) {
				if (c.getId().equals(i.getElementAddition())) {
					name1 = "Caso de Uso " + c.getName();
				}

				if (c.getId().equals(i.getElementBase())) {
					name2 = "Caso de Uso " + c.getName();
				}
			}

			resposta.append(name2 + " Inclui o " + name1 + "\n");
		}
		if (listaextend.size() > 0)
			resposta.append("Extends:\n");
		for (Extend extendSelecionado : listaextend) {

			// System.out.println(a.getIdElemento1()+" "+a.getIdElemento2());
			String name1 = "";
			String name2 = "";

			for (UseCase c : casos) {
				if (c.getId().equals(extendSelecionado.getElementBase())) {
					name1 = "Caso de Uso " + c.getName();
				}

				if (c.getId().equals(extendSelecionado.getElementExtension())) {
					name2 = "Caso de Uso " + c.getName();
				}
			}

			resposta.append(name1 + " Estende o " + name2 + "\n");
		}
		if (generalizacoes.size() > 0)
			resposta.append("Generalizações :\n");
		for (Generalization a : generalizacoes) {

			// System.out.println(a.getIdElemento1()+" "+a.getIdElemento2());
			String name1 = "";
			String name2 = "";

			for (Actor at : atores) {
				if (at.getId().equals(a.getElementChild())) {
					name1 = "Ator " + at.getName();
				}

				if (at.getId().equals(a.getElementParent())) {
					name2 = "Ator " + at.getName();
				}
			}
			for (UseCase c : casos) {
				if (c.getId().equals(a.getElementChild())) {
					name1 = "Caso de Uso " + c.getName();
				}

				if (c.getId().equals(a.getElementParent())) {
					name2 = "Caso de Uso " + c.getName();
				}
			}

			resposta.append(name1 + " herda o " + name2 + "\n");
		}
		if (notes.size() > 0)
			resposta.append("Notas :\n");
		for (Note note : notes) {

			resposta.append("Nota: " + note.getBody() + "\n");
			if (note.getIdAssociados().size() == 0) {
				resposta.append("Sem referencia\n");
			} else {
				resposta.append("Referente a:\n");
				for (String id : note.getIdAssociados()) {
					resposta.append("\t");
					for (Actor at : atores) {
						if (at.getId().equals(id)) {
							resposta.append("Ator ").append(at.getName());
							break;
						}
					}
					for (UseCase c : casos) {
						if (c.getId().equals(id)) {
							resposta.append("Caso de Uso ").append(c.getName());
							break;
						}
					}
					for (Association a : associacoes) {
						if (a.getId().equals(id)) {
							resposta.append("Associação entre ");
							String nome1 = "", nome2 = "";
							for (ElementAssociation e : a.getElementList()) {
								for (Actor at : atores) {
									if (at.getId().equals(e.getIdParticipant())) {
										if (nome1.equals(""))
											nome1 = "Ator " + at.getName();
										else
											nome2 = "Ator " + at.getName();

									}
								}
								for (UseCase ca : casos) {
									if (ca.getId().equals(e.getIdParticipant())) {
										if (nome1.equals(""))
											nome1 = "Caso de Uso "
													+ ca.getName();
										else
											nome2 = "Caso de Uso "
													+ ca.getName();

									}
								}
							}
							resposta.append(nome1 + " e " + nome2 + "\n");
							break;
						}
					}
					for (Extend e : listaextend) {
						if (e.getId().equals(id)) {
							resposta.append("Extend entre ");
							String nome1 = "", nome2 = "";

							for (UseCase ca : casos) {
								if (ca.getId().equals(e.getElementBase()))
									nome1 = "Caso de Uso " + ca.getName();

								if (ca.getId().equals(e.getElementExtension()))
									nome2 = "Caso de Uso " + ca.getName();
							}

							resposta.append(nome1 + " e " + nome2 + "\n");
							break;
						}
					}
					for (Include i : includes) {
						if (i.getId().equals(id)) {
							resposta.append("Include entre ");
							String nome1 = "", nome2 = "";

							for (UseCase ca : casos) {
								if (ca.getId().equals(i.getElementBase()))
									nome1 = "Caso de Uso " + ca.getName();

								if (ca.getId().equals(i.getElementAddition()))
									nome2 = "Caso de Uso " + ca.getName();
							}

							resposta.append(nome1 + " e " + nome2 + "\n");
							break;
						}
					}
					for (Generalization g : generalizacoes) {
						if (g.getId().equals(id)) {
							resposta.append("Generalização entre ");
							String nome1 = "", nome2 = "";

							for (Actor at : atores) {
								if (at.getId().equals(g.getElementChild()))
									nome1 = "Ator " + at.getName();

								if (at.getId().equals(g.getElementParent()))
									nome2 = "Ator " + at.getName();
							}

							for (UseCase ca : casos) {
								if (ca.getId().equals(g.getElementChild()))
									nome1 = "Caso de Uso " + ca.getName();

								if (ca.getId().equals(g.getElementParent()))
									nome2 = "Caso de Uso " + ca.getName();
							}

							resposta.append(nome1 + " e " + nome2 + "\n");
							break;
						}
					}
					resposta.append("\n");
				}
			}

		}
		return resposta;
	}
}
