package com.br.fatecmc.case_b.use_case;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.JDOMException;

import com.br.fatecmc.case_b.componentes_uml.Actor;
import com.br.fatecmc.case_b.componentes_uml.Association;
import com.br.fatecmc.case_b.componentes_uml.ElementAssociation;
import com.br.fatecmc.case_b.componentes_uml.Extend;
import com.br.fatecmc.case_b.componentes_uml.Generalization;
import com.br.fatecmc.case_b.componentes_uml.Include;
import com.br.fatecmc.case_b.componentes_uml.Note;
import com.br.fatecmc.case_b.componentes_uml.UseCase;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.impl.ProjectImpl;
import com.br.fatecmc.case_b.impl.XMIWriterFactoryImpl;
import com.br.fatecmc.case_b.interfaces.XMIWriter;

public class ViewUseCase {

	static List<Actor> atores;
	static List<UseCase> casos;
	static List<Association> associacoes;
	static List<Include> includes;
	static List<Extend> listaextend;
	static List<Generalization> generalizacoes;
	static List<Note> notes;
	static int contadorElementos = 1;

	public static void clear() {
		atores.clear();
		casos.clear();
		associacoes.clear();
		includes.clear();
		listaextend.clear();
		generalizacoes.clear();
		notes.clear();
		contadorElementos = 1;
	}

	public static void main(String[] args) throws IOException, JDOMException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		atores = new ArrayList<Actor>();
		casos = new ArrayList<UseCase>();
		associacoes = new ArrayList<Association>();
		listaextend = new ArrayList<Extend>();
		includes = new ArrayList<Include>();
		generalizacoes = new ArrayList<Generalization>();
		notes = new ArrayList<Note>();

		int respostaUsuario = 0;
		String nomeDoProjeto = "semNome";
		String entrada = "";
		boolean flgSair = true;
		boolean flgMenuPrincipal = true;
		ReadUseCase ler = new ReadUseCase();
		LoadUseCase carregar = new LoadUseCase();
		while (flgSair) {

			System.out.println("Bem vindo - UML4VI ");
			System.out.println("1 - Criar novo Diagrama de Caso de Uso");
			System.out.println("2 - Carregar um diagrama já existente");
			System.out.println("3 - Finalizar");
			flgMenuPrincipal = true;

			switch (respostaUsuario = lerInteiroValido()) {

			// criando Atores

			case 1:
				clear();
				System.out.println("Insira o nome do Diagrama");
				entrada = in.readLine();
				nomeDoProjeto = entrada;
				break;

			// criando Caso de usos.
			case 2:
				clear();
				System.out.println("Insira o caminho com o nome do arquivo");
				entrada = in.readLine();
				contadorElementos = carregar.carregarDados(entrada, atores,
						casos, associacoes, includes, listaextend,
						generalizacoes, notes);
				nomeDoProjeto = entrada;
				break;
			case 3:
				System.out.println("Até a próxima.");
				flgSair = flgMenuPrincipal = false;
				break;
			default:
				System.out.println("Opção não encontrada");
				break;
			}

			while (flgMenuPrincipal) {

				boolean inserir = true;
				boolean editar = true;
				boolean remover = true;

				System.out.println("Menu Principal - UML4VI ");
				System.out.println("1 - Inserir novos Elementos");
				System.out.println("2 - Editar Elementos");
				System.out.println("3 - Excluir Elemento");
				System.out.println("4 - Ler o Diagrama");
				System.out.println("5 - Salvar");
				System.out.println("6 - Finalizar");
				System.out.println("7 - Voltar");
				switch (respostaUsuario = lerInteiroValido()) {
				// criando Atores
				case 1:
					while (inserir) {
						System.out.println("Criação de Elementos - UML4VI ");
						System.out.println("1 - Criar Ator");
						System.out.println("2 - Criar Caso de Uso");
						System.out.println("3 - Criar Associação");
						System.out.println("4 - Criar Extend");
						System.out.println("5 - Criar Include");
						System.out.println("6 - Criar Generalização");
						System.out.println("7 - Voltar");
						switch (respostaUsuario = lerInteiroValido()) {
						// criando Atores
						case 1:
							System.out
									.println("Insira o nome do Ator (c para cancelar) ");
							entrada = in.readLine();
							if (!entrada.toLowerCase().equals("c")) {
								atores.add(new Actor(String
										.valueOf(contadorElementos++), entrada));
							}
							break;

						// criando Caso de usos.
						case 2:
							System.out
									.println("Insira o nome do Caso de uso ( c para cancelar ) ");
							entrada = in.readLine();
							if (!entrada.toLowerCase().equals("c")) {
								casos.add(new UseCase(String
										.valueOf(contadorElementos++), entrada));
							}
							break;

						// criando Associções
						case 3:
							Association associacao = new Association();
							int contadorGenerico = 0;
							do {
								contadorGenerico = -1;
								System.out.println("Atores:");
								for (Actor a : atores) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());

								}
								System.out
										.println("Selecione o ator conforme seu numero correspondente (c para cancelar)");
								entrada = (in.readLine());
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}

							ElementAssociation el = new ElementAssociation();
							el.setIdParticipant(atores.get(respostaUsuario)
									.getId());
							associacao.addElement(el);

							do {
								contadorGenerico = -1;
								System.out.println("Caso de Uso:");
								for (UseCase a : casos) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o Caso de uso conforme seu numero correspondente (c para cancelar)");
								entrada = (in.readLine());
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));

							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							ElementAssociation el2 = new ElementAssociation();
							el2.setIdParticipant(casos.get(respostaUsuario)
									.getId());
							associacao.addElement(el2);
							associacoes.add(associacao);
							break;
						// Extend
						case 4:
							Extend extend = new Extend();
							contadorGenerico = 0;
							do {
								contadorGenerico = -1;
								System.out.println("Caso de Uso:");
								for (UseCase a : casos) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o ator conforme seu numero correspondente ( c para cancelar)");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							extend.setElementBase(casos.get(respostaUsuario)
									.getId());

							do {
								contadorGenerico = -1;
								System.out.println("Caso de Uso:");
								for (UseCase a : casos) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o Caso de uso conforme seu numero correspondente (c para cancelar)");
								entrada = in.readLine();

								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							extend.setElementExtension(casos.get(
									respostaUsuario).getId());
							listaextend.add(extend);
							break;

						// include
						case 5:
							Include include = new Include();
							contadorGenerico = 0;
							do {
								contadorGenerico = -1;
								System.out.println("Caso de Uso:");
								for (UseCase a : casos) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o ator conforme seu numero correspondente (c para cancelar)");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							include.setElementBase(casos.get(respostaUsuario)
									.getId());

							do {
								contadorGenerico = -1;
								System.out.println("Caso de Uso:");
								for (UseCase a : casos) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o Caso de uso conforme seu numero correspondente (c para cancelar)");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							include.setElementAddition(casos.get(
									respostaUsuario).getId());
							includes.add(include);
							break;

						// criando Generalização
						case 6:
							contadorGenerico = 0;
							Generalization generalizacao = new Generalization();
							do {
								contadorGenerico = -1;
								System.out.println("Ator (filho):");
								for (Actor a : atores) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o ator conforme seu numero correspondente (c para cancelar)");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							generalizacao.setElementChild(atores.get(
									respostaUsuario).getId());

							do {
								contadorGenerico = -1;
								System.out.println("Ator (Pai):");
								for (Actor a : atores) {
									contadorGenerico++;
									System.out.println(contadorGenerico + " - "
											+ a.getName());
								}
								System.out
										.println("Selecione o ator conforme seu numero correspondente (c para cancelar)");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
								respostaUsuario = Integer.parseInt(entrada);
							} while (respostaUsuario < 0
									&& respostaUsuario > (contadorGenerico));
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							generalizacao.setElementParent(atores.get(
									respostaUsuario).getId());
							generalizacoes.add(generalizacao);
							break;

						case 7:
							inserir = false;
							break;
						default:
							System.out.println("Opção não encontrada");
						}
					}
					break;

				case 2:
					while (editar) {

						System.out.println("Editor de Elementos - UML4VI ");
						System.out.println("1 - Editar Ator");
						System.out.println("2 - Editar Caso de Uso");
						System.out.println("7 - Voltar");
						switch (respostaUsuario = lerInteiroValido()) {
						// Editando Atores
						case 1:
							int contadorAt = -1;
							do {
								System.out.println("Atores: \n");
								contadorAt = -1;
								for (Actor a : atores) {
									contadorAt++;
									System.out.println(contadorAt + " - "
											+ a.getName());
								}
								System.out
										.println("Escolha a opção conforme deseja alterar ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorAt);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							System.out.println("Insira o novo nome do Ator "
									+ atores.get(Integer.parseInt(entrada))
											.getName());
							atores.get(Integer.parseInt(entrada)).setName(
									in.readLine());
							break;

						// criando Caso de usos.
						case 2:
							int contadorCa = -1;
							do {
								System.out.println("Caso de Uso: \n");
								contadorCa = -1;
								for (UseCase c : casos) {
									contadorCa++;
									System.out.println(contadorCa + " - "
											+ c.getName());
								}
								System.out
										.println("Escolha a opção conforme deseja alterar ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}
							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorCa);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							System.out
									.println("Insira o novo nome do Caso de Uso "
											+ casos.get(
													Integer.parseInt(entrada))
													.getName());
							casos.get(Integer.parseInt(entrada)).setName(
									in.readLine());
							break;

						case 7:
							editar = false;
							break;
						default:
							System.out.println("Opção não encontrada");
						}
					}
					break;
				case 3:
					while (remover) {

						System.out.println("Remoção de Elementos - UML4VI  ");
						System.out.println("1 - Remover Ator");
						System.out.println("2 - Remover Caso de Uso");
						System.out.println("3 - Remover Associação");
						System.out.println("4 - Remover Extend");
						System.out.println("5 - Remover Include");
						System.out.println("6 - Remover Generalização");
						System.out.println("7 - Voltar");
						switch (respostaUsuario = lerInteiroValido()) {
						// Removendo Atores
						case 1:
							int contadorAt = -1;
							do {
								System.out.println("Atores: \n");
								contadorAt = -1;
								for (Actor a : atores) {
									contadorAt++;
									System.out.println(contadorAt + " - "
											+ a.getName());
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorAt);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							List<Integer> codigos = new ArrayList<Integer>();
							int codigoRemove = -1;
							for (Association ass : associacoes) {
								codigoRemove++;
								if (ass.getElementList()
										.get(0)
										.getIdParticipant()
										.equals(atores.get(
												Integer.parseInt(entrada))
												.getId())
										|| ass.getElementList()
												.get(1)
												.getIdParticipant()
												.equals(atores
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								associacoes.remove(c);
							}
							codigos.clear();
							codigoRemove = -1;
							for (Generalization g : generalizacoes) {
								codigoRemove++;
								if (g.getElementChild().equals(
										atores.get(Integer.parseInt(entrada))
												.getId())
										|| g.getElementParent()
												.equals(atores
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								generalizacoes.remove(c);
							}

							atores.remove(Integer.parseInt(entrada));

							break;

						// criando Caso de usos.
						case 2:
							int contadorUC = -1;
							do {
								System.out.println("Caso de Uso: \n");
								contadorUC = -1;
								for (UseCase a : casos) {
									contadorUC++;
									System.out.println(contadorUC + " - "
											+ a.getName());
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorUC);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							codigos = new ArrayList<Integer>();
							codigos.clear();
							codigoRemove = -1;
							for (Association ass : associacoes) {
								codigoRemove++;
								if (ass.getElementList()
										.get(0)
										.getIdParticipant()
										.equals(casos.get(
												Integer.parseInt(entrada))
												.getId())
										|| ass.getElementList()
												.get(1)
												.getIdParticipant()
												.equals(casos
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								associacoes.remove(c);
							}
							codigos.clear();
							codigoRemove = -1;
							for (Generalization g : generalizacoes) {
								codigoRemove++;
								if (g.getElementChild().equals(
										casos.get(Integer.parseInt(entrada))
												.getId())
										|| g.getElementParent()
												.equals(casos
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								generalizacoes.remove(c);
							}

							codigos.clear();
							codigoRemove = -1;
							for (Extend g : listaextend) {
								codigoRemove++;
								if (g.getElementBase().equals(
										casos.get(Integer.parseInt(entrada))
												.getId())
										|| g.getElementExtension()
												.equals(casos
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								listaextend.remove(c);
							}

							codigos.clear();
							codigoRemove = -1;
							for (Include g : includes) {
								codigoRemove++;
								if (g.getElementBase().equals(
										casos.get(Integer.parseInt(entrada))
												.getId())
										|| g.getElementAddition()
												.equals(casos
														.get(Integer
																.parseInt(entrada))
														.getId())) {
									codigos.add(codigoRemove);
								}
							}
							for (int c : codigos) {
								includes.remove(c);
							}

							casos.remove(Integer.parseInt(entrada));
							break;

						// criando Associações
						case 3:
							int contadorAs = -1;
							do {
								System.out.println("Associações: \n");
								contadorAs = -1;
								List<String> nomes = new ArrayList<String>();
								String nome1 = "";
								String nome2 = "";
								for (Association a : associacoes) {
									nome1 = "";
									nome2 = "";
									for (ElementAssociation element : a
											.getElementList()) {

										for (Actor actor : atores) {
											if (element.getIdParticipant()
													.equals(actor.getId())) {
												if (nome1.equals(""))
													nome1 = "Ator "
															+ actor.getName();
												else
													nome2 = "Ator "
															+ actor.getName();

											}
										}
										for (UseCase uc : casos) {
											if (element.getIdParticipant()
													.equals(uc.getId())) {
												if (nome1.equals(""))
													nome1 = "Caso de Uso "
															+ uc.getName();
												else
													nome2 = "Caso de Uso "
															+ uc.getName();

											}
										}
									}
									nomes.add(nome1 + " e " + nome2);
								}
								for (String descricao : nomes) {
									contadorAs++;
									System.out.println(contadorAs + " - Entre "
											+ descricao);
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorAs);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							associacoes.remove(Integer.parseInt(entrada));
							break;
						// Extend
						case 4:
							int contadorEx = -1;
							do {
								System.out.println("Extends: \n");
								contadorEx = -1;
								List<String> nomes = new ArrayList<String>();
								String nome1 = "";
								String nome2 = "";
								for (Extend a : listaextend) {
									for (Actor actor : atores) {
										if (a.getElementBase().equals(
												actor.getId())) {
											nome1 = "Ator " + actor.getName();
										}
										if (a.getElementExtension().equals(
												actor.getId())) {
											nome2 = "Ator " + actor.getName();
										}

									}
									for (UseCase uc : casos) {
										if (a.getElementBase().equals(
												uc.getId())) {
											nome1 = "Caso de Uso "
													+ uc.getName();
										}
										if (a.getElementExtension().equals(
												uc.getId())) {
											nome2 = "Caso de Uso "
													+ uc.getName();
										}
									}

									nomes.add(nome1 + " e " + nome2);
								}
								for (String descricao : nomes) {
									contadorEx++;
									System.out.println(contadorEx + " - Entre "
											+ descricao);
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorEx);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							listaextend.remove(Integer.parseInt(entrada));
							break;

						// include
						case 5:
							int contadorIn = -1;
							do {
								System.out.println("Includes: \n");
								contadorIn = -1;
								List<String> nomes = new ArrayList<String>();
								String nome1 = "";
								String nome2 = "";
								for (Include a : includes) {
									for (Actor actor : atores) {
										if (a.getElementBase().equals(
												actor.getId())) {
											nome1 = "Ator " + actor.getName();
										}
										if (a.getElementAddition().equals(
												actor.getId())) {
											nome2 = "Ator " + actor.getName();
										}

									}
									for (UseCase uc : casos) {
										if (a.getElementBase().equals(
												uc.getId())) {
											nome1 = "Caso de Uso "
													+ uc.getName();
										}
										if (a.getElementAddition().equals(
												uc.getId())) {
											nome2 = "Caso de Uso "
													+ uc.getName();
										}
									}

									nomes.add(nome1 + " e " + nome2);
								}
								for (String descricao : nomes) {
									contadorIn++;
									System.out.println(contadorIn + " - Entre "
											+ descricao);
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorIn);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							includes.remove(Integer.parseInt(entrada));
							break;

						// criando Generalização
						case 6:
							int contadorGe = -1;
							do {
								System.out.println("Generalizações: \n");
								contadorGe = -1;
								List<String> nomes = new ArrayList<String>();
								String nome1 = "";
								String nome2 = "";
								for (Generalization a : generalizacoes) {
									for (Actor actor : atores) {
										if (a.getElementChild().equals(
												actor.getId())) {
											nome1 = "Ator " + actor.getName();
										}
										if (a.getElementParent().equals(
												actor.getId())) {
											nome2 = "Ator " + actor.getName();
										}

									}
									for (UseCase uc : casos) {
										if (a.getElementChild().equals(
												uc.getId())) {
											nome1 = "Caso de Uso "
													+ uc.getName();
										}
										if (a.getElementParent().equals(
												uc.getId())) {
											nome2 = "Caso de Uso "
													+ uc.getName();
										}
									}

									nomes.add(nome1 + " e " + nome2);
								}
								for (String descricao : nomes) {
									contadorGe++;
									System.out.println(contadorGe + " - Entre "
											+ descricao);
								}
								System.out
										.println("Escolha a opção conforme deseja remover ou c para cancelar ");
								entrada = in.readLine();
								if (entrada.toLowerCase().equals("c")) {
									break;
								}

							} while (Integer.parseInt(entrada) < 0
									&& Integer.parseInt(entrada) > contadorGe);
							if (entrada.toLowerCase().equals("c")) {
								break;
							}
							generalizacoes.remove(Integer.parseInt(entrada));
							break;

						case 7:
							remover = false;
							break;
						default:
							System.out.println("Opção não encontrada");
						}
					}
					break;

				case 4:
					System.out.println("\n"
							+ ler.leitura(atores, casos, associacoes, includes,
									listaextend, generalizacoes, notes)
									.toString());
					System.out.println("\nDigite Enter para continuar");
					entrada = in.readLine();

					break;

				case 5:
					ProjectImpl project = new ProjectImpl();
					project.setName(nomeDoProjeto);
					project.setVersion("1.2");

					for (Actor a : atores) {
						com.br.fatecmc.case_b.domain.Actor act = new com.br.fatecmc.case_b.domain.Actor();
						act.setId(a.getId());
						act.setName(a.getName());
						project.getActorList().add(act);
					}

					for (UseCase a : casos) {
						com.br.fatecmc.case_b.domain.UseCase uc = new com.br.fatecmc.case_b.domain.UseCase();
						uc.setId(a.getId());
						uc.setName(a.getName());
						project.getUseCaseList().add(uc);
					}

					for (Association ass : associacoes) {

						com.br.fatecmc.case_b.domain.Actor associaActor = null;
						com.br.fatecmc.case_b.domain.UseCase associaUse = null;
						// element 0 actor
						for (com.br.fatecmc.case_b.domain.Actor a : project
								.getActorList()) {
							if (ass.getElementList().get(0).getIdParticipant()
									.equals(a.getId())) {
								associaActor = a;
								break;
							}
						}
						// element 1 caseUse
						for (com.br.fatecmc.case_b.domain.UseCase u : project
								.getUseCaseList()) {
							if (ass.getElementList().get(1).getIdParticipant()
									.equals(u.getId())) {
								associaUse = u;
								break;
							}
						}

						Relationship relation = new Relationship();
						relation.setId(contadorElementos++ + "x");
						relation.setRelationshipType(RelationshipType.Association);
						relation.getRelationshipComponentList().add(
								associaActor);

						relation.getRelationshipComponentList().add(associaUse);
						associaActor.getRelationshipList().add(relation);
						associaUse.getRelationshipList().add(relation);

					}

					for (Include ass : includes) {

						com.br.fatecmc.case_b.domain.UseCase associaBase = null;
						com.br.fatecmc.case_b.domain.UseCase associaAdd = null;
						// element 0 caseUse
						for (com.br.fatecmc.case_b.domain.UseCase a : project
								.getUseCaseList()) {
							if (ass.getElementBase().equals(a.getId())) {
								associaBase = a;
								break;
							}
						}
						// element 1 caseUse
						for (com.br.fatecmc.case_b.domain.UseCase u : project
								.getUseCaseList()) {
							if (ass.getElementAddition().equals(u.getId())) {
								associaAdd = u;
								break;
							}
						}

						Relationship relation = new Relationship();
						relation.setId(contadorElementos++ + "x");
						relation.setRelationshipType(RelationshipType.Include);

						relation.getRelationshipComponentList().add(associaAdd);
						relation.getRelationshipComponentList()
								.add(associaBase);
						associaBase.getRelationshipList().add(relation);
						associaAdd.getRelationshipList().add(relation);

					}

					for (Extend ass : listaextend) {

						com.br.fatecmc.case_b.domain.UseCase associaBase = null;
						com.br.fatecmc.case_b.domain.UseCase associaAdd = null;
						// element 0 caseUse
						for (com.br.fatecmc.case_b.domain.UseCase a : project
								.getUseCaseList()) {
							if (ass.getElementBase().equals(a.getId())) {
								associaBase = a;
								break;
							}
						}
						// element 1 caseUse
						for (com.br.fatecmc.case_b.domain.UseCase u : project
								.getUseCaseList()) {
							if (ass.getElementExtension().equals(u.getId())) {
								associaAdd = u;
								break;
							}
						}

						Relationship relation = new Relationship();
						relation.setId(contadorElementos++ + "x");
						relation.setRelationshipType(RelationshipType.Extend);

						relation.getRelationshipComponentList()
								.add(associaBase);
						relation.getRelationshipComponentList().add(associaAdd);
						associaBase.getRelationshipList().add(relation);
						associaAdd.getRelationshipList().add(relation);

					}

					for (Generalization ass : generalizacoes) {

						com.br.fatecmc.case_b.domain.Actor actorChild = null;
						com.br.fatecmc.case_b.domain.Actor actorParent = null;
						// element 0 caseUse
						for (com.br.fatecmc.case_b.domain.Actor a : project
								.getActorList()) {
							if (ass.getElementChild().equals(a.getId())) {
								actorChild = a;
								break;
							}
						}
						// element 1 caseUse
						for (com.br.fatecmc.case_b.domain.Actor u : project
								.getActorList()) {
							if (ass.getElementParent().equals(u.getId())) {
								actorParent = u;
								break;
							}
						}

						Relationship relation = new Relationship();
						relation.setId(contadorElementos++ + "x");
						relation.setRelationshipType(RelationshipType.Generalization);

						relation.getRelationshipComponentList().add(actorChild);
						relation.getRelationshipComponentList()
								.add(actorParent);
						actorChild.getRelationshipList().add(relation);
						actorParent.getRelationshipList().add(relation);

					}
					// CreateUseCase criar = new CreateUseCase();
					// XMLOutputter xout = new XMLOutputter();
					//
					XMIWriterFactoryImpl factory = new XMIWriterFactoryImpl();
					// ArgoUMLXMIWriter writer = new ArgoUMLXMIWriter();
					XMIWriter writer = factory.create("argouml", "1.2");
					Long reposta;
					if (nomeDoProjeto.length() > 3
							&& nomeDoProjeto.substring(
									nomeDoProjeto.length() - 4,
									nomeDoProjeto.length() - 3).equals(".")) {
						OutputStream out = new FileOutputStream(new File(
								nomeDoProjeto));
						reposta = writer.writeProjectTo(out, project);
						// factory.create(toolName, toolVersion)
						// xout.output(criar.criarCasoDeUso(nomeDoProjeto,
						// atores, casos, associacoes, includes, listaextend,
						// generalizacoes,contadorElementos), out);

					} else {
						OutputStream out = new FileOutputStream(new File(
								nomeDoProjeto + ".xmi"));
						reposta = writer.writeProjectTo(out, project);
						// xout.output(criar.criarCasoDeUso(nomeDoProjeto,
						// atores, casos, associacoes, includes, listaextend,
						// generalizacoes,contadorElementos), out);
					}
					if (reposta == 1) {
						System.out.println("Salvo com sucesso.");
					} else {
						System.out.println("Erro ao salvar");
					}
					break;

				case 6:
					System.out.println("Até a próxima.");
					flgMenuPrincipal = false;
					flgSair = false;
					break;

				case 7:
					flgMenuPrincipal = false;
					break;
				default:
					System.out.println("Opção não encontrada");
					break;

				}
			}

		}

	}

	/**
	 * Le uma linha do console e retorna um inteiro
	 * 
	 * @return Um inteiro que foi lido da entrada padrao (console), -1 se foi
	 *         informado um texto ou vazio
	 * @author Girdacio
	 */
	private static int lerInteiroValido() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String entrada;
		int opcao;

		try {
			entrada = in.readLine();
			if (entrada.equals(""))
				return -1;
			else {
				try {
					opcao = Integer.parseInt(entrada);
					return opcao;
				} catch (java.lang.NumberFormatException ex) {
					return -1;
				}
			}
		} catch (IOException e) {
			return -1;
		}
	}
}
