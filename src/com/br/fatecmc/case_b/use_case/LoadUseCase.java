package com.br.fatecmc.case_b.use_case;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import com.br.fatecmc.case_b.componentes_uml.Actor;
import com.br.fatecmc.case_b.componentes_uml.Association;
import com.br.fatecmc.case_b.componentes_uml.ElementAssociation;
import com.br.fatecmc.case_b.componentes_uml.Extend;
import com.br.fatecmc.case_b.componentes_uml.Generalization;
import com.br.fatecmc.case_b.componentes_uml.Include;
import com.br.fatecmc.case_b.componentes_uml.Note;
import com.br.fatecmc.case_b.componentes_uml.UseCase;

/**
 * 
 * @author Erico Veriscimo
 */
public class LoadUseCase {
	static Element consulta;
	static int contador = 1;

	public int carregarDados(String path, List<Actor> actors,
			List<UseCase> cases, List<Association> associations,
			List<Include> includes, List<Extend> extend_s,
			List<Generalization> generalizations, List<Note> notes)
			throws JDOMException, IOException {
		// Tenta processar o arquivo de configuração e procurar pela ação.
		SAXBuilder builder = new SAXBuilder();
		Document documento = builder.build(new File(path));

		// Recupera uma referencia para o elemento raiz.
		Element elementoRaiz = documento.getRootElement();
		// Recupera e itera pelos filhos.
		// List<Element> listaAcoes = elementoRaiz.getChildren();

		try {
			RetornaConsulta(elementoRaiz, "Namespace.ownedElement");

		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		for (Element e : consulta.getChildren()) {
			if (e.getName().equals("UseCase")) {
				contador++;
				cases.add(new UseCase(e.getAttributeValue("xmi.id"), e
						.getAttributeValue("name")));
			}
			if (e.getName().equals("Actor")) {
				contador++;
				actors.add(new Actor(e.getAttributeValue("xmi.id"), e
						.getAttributeValue("name")));
			}
			if (e.getName().equals("Association")) {
				contador++;
				Association a = new Association();
				a.setId(e.getAttributeValue("xmi.id"));
				Element associacao = null;
				for (Element eee : e.getChildren()) {
					// System.out.println(eee.getName());
					if (eee.getName().equals("Association.connection")) {
						associacao = eee;
						break;
					}
				}
				if (associacao != null) {

					for (Element AssociationEnd : associacao.getChildren()) {

						if (AssociationEnd.getName().equals("AssociationEnd")) {
							ElementAssociation element = new ElementAssociation();
							element.setNavigable(Boolean
									.parseBoolean(AssociationEnd
											.getAttributeValue("isNavigable")));

							for (Element AssociationEndparticipant : AssociationEnd
									.getChildren()) {

								if (AssociationEndparticipant.getName().equals(
										"AssociationEnd.multiplicity")) {
									for (Element multiplicityRage : AssociationEndparticipant
											.getChildren()) {
										element.setIdMutiplicity(multiplicityRage
												.getAttributeValue("xmi.id"));
										element.setMutiplicityLower(multiplicityRage
												.getAttributeValue("lower"));
										element.setMutiplicityUpper(multiplicityRage
												.getAttributeValue("upper"));

										break;
									}
								}
								if (AssociationEndparticipant.getName().equals(
										"AssociationEnd.participant")) {
									Element participante = null;
									for (Element procurandoParticipante : AssociationEndparticipant
											.getChildren()) {
										participante = procurandoParticipante;
										break;
									}

									element.setIdParticipant(participante
											.getAttributeValue("xmi.idref"));
									a.addElement(element);

								}

							}
						}
					}

					associations.add(a);
				}
			}
			if (e.getName().equals("Include")) {
				contador++;
				Include i = new Include();
				i.setId(e.getAttributeValue("xmi.id"));
				for (Element filhosInclude : e.getChildren()) {
					if (filhosInclude.getName().equals("Include.addition")) {
						for (Element participante : filhosInclude.getChildren()) {
							i.setElementAddition(participante
									.getAttributeValue("xmi.idref"));
						}
					}
					if (filhosInclude.getName().equals("Include.base")) {
						for (Element participante : filhosInclude.getChildren()) {
							i.setElementBase(participante
									.getAttributeValue("xmi.idref"));
						}
					}

				}
				includes.add(i);
			}

			if (e.getName().equals("Extend")) {
				contador++;
				Extend extend = new Extend();
				extend.setId(e.getAttributeValue("xmi.id"));
				for (Element filhosExtend : e.getChildren()) {
					if (filhosExtend.getName().equals("Extend.extension")) {
						for (Element participante : filhosExtend.getChildren()) {
							extend.setElementExtension(participante
									.getAttributeValue("xmi.idref"));
						}
					}
					if (filhosExtend.getName().equals("Extend.base")) {
						for (Element participante : filhosExtend.getChildren()) {
							extend.setElementBase(participante
									.getAttributeValue("xmi.idref"));
						}
					}

				}
				extend_s.add(extend);
			}

			if (e.getName().equals("Generalization")) {
				contador++;
				Generalization g = new Generalization();
				g.setId(e.getAttributeValue("xmi.id"));
				for (Element filhosGeneralizacao : e.getChildren()) {
					if (filhosGeneralizacao.getName().equals(
							"Generalization.parent")) {
						for (Element participante : filhosGeneralizacao
								.getChildren()) {
							g.setElementParent(participante
									.getAttributeValue("xmi.idref"));
						}
					}
					if (filhosGeneralizacao.getName().equals(
							"Generalization.child")) {
						for (Element participante : filhosGeneralizacao
								.getChildren()) {
							g.setElementChild(participante
									.getAttributeValue("xmi.idref"));
						}
					}

				}
				generalizations.add(g);
			}
			
			if(e.getName().equals("Comment")){
				Note note = new Note();
				note.setId(e.getAttributeValue("xmi.id"));
				note.setBody(e.getAttributeValue("body"));
				note.setSpecification(Boolean.parseBoolean(e.getAttributeValue("isSpeccification")));
				for(Element Comment : e.getChildren()){
					for(Element asso : Comment.getChildren()){
						note.addIdAssociation(asso.getAttributeValue("xmi.idref"));
						break;
					}
					break;
				}
				notes.add(note);
			}

		}
		return contador;
	}

	public static void RetornaConsulta(Element raiz, String tag)
			throws Exception {
		// System.out.println(raiz.getName());
		if (raiz == null) {
			Exception erro = new Exception("Raiz Nula");
			throw erro;

		}
		if (raiz.getName().equals(tag)) {
			consulta = raiz;
			Exception done = new Exception("Tag encontrada");

			throw done;
		}
		for (Element e : raiz.getChildren()) {
			RetornaConsulta(e, tag);
		}

	}
}
