package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLIncludeParser implements CommandParser{

	@Override
	public Project parser(Project project, Element element) {
		
		if (element.getName().equals("Include")) {
			Relationship relationship = new Relationship();
			relationship.setId(element.getAttributeValue("xmi.id"));
			relationship.setRelationshipType(RelationshipType.Include);
			
			RelationshipComponent componentAddition = null;
			RelationshipComponent componentBase = null;
			
			for (Element filhosInclude : element.getChildren()) {
				if (filhosInclude.getName().equals("Include.addition")) {
					for (Element participante : filhosInclude.getChildren()) {
						componentAddition = project.searchUseCase(participante
								.getAttributeValue("xmi.idref"));
						
					}
				}
				if (filhosInclude.getName().equals("Include.base")) {
					for (Element participante : filhosInclude.getChildren()) {
						componentBase = project.searchUseCase(participante
								.getAttributeValue("xmi.idref"));
			
					}
				}

			}
			relationship.getRelationshipComponentList().add(componentAddition);
			relationship.getRelationshipComponentList().add(componentBase);
			
			componentAddition.getRelationshipList().add(relationship);
			componentBase.getRelationshipList().add(relationship);
		}
		return project;
	}

}
