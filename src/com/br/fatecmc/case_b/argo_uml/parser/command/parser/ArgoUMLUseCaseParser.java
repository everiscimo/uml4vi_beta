package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.UseCase;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLUseCaseParser implements CommandParser{

	@Override
	public Project parser(Project project, Element element) {
		if (element.getName().equals("UseCase")) {
			UseCase useCase = new UseCase(); 
			useCase.setId(element.getAttributeValue("xmi.id"));
			useCase.setName(element.getAttributeValue("name"));
			
			project.getUseCaseList().add(useCase);
		}			
		
		return project;
	}

}
