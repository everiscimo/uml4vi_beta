package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Note;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.domain.UMLElement;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLCommentParser implements CommandParser{

	@Override
	public Project parser(Project project, Element element) {
		if(element.getName().equals("Comment")){
			
			
			Note note = project.searchNote(element.getAttributeValue("xmi.id"));

			for(Element Comment : element.getChildren()){
				
				for(Element association : Comment.getChildren()){
					Relationship relationship = new Relationship();
					relationship.setRelationshipType(RelationshipType.Comment);
					relationship.setId(element.getAttributeValue("xmi.id"));
					relationship.getRelationshipComponentList().add(note);
					// buscando um componente UML para a associa��o
					UMLElement UMLelement = project.searchAll(association
							.getAttributeValue("xmi.idref"));
					
					//verificando de o relacionamento � um relacionamento(Ex: Associate, Extends)
					if(UMLelement instanceof Relationship){
						for(RelationshipComponent relationshipComponent : ((Relationship) UMLelement).getRelationshipComponentList()){
							relationship.getRelationshipComponentList().add(relationshipComponent);	
						}
						for(RelationshipComponent relationshipComponent : relationship.getRelationshipComponentList()){
							relationshipComponent.getRelationshipList().add(relationship);
						}
						note.getRelationshipList().add(relationship);
					}
					else{
						relationship.getRelationshipComponentList().add((RelationshipComponent) UMLelement);
						((RelationshipComponent) UMLelement).getRelationshipList().add(relationship);
						note.getRelationshipList().add(relationship);
					}
					
					
					break;
				}
					
				
			}
				
			
			project.getNoteList().add(note);
		}	
		
		
			return project;
	}


}
