package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Actor;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLActorParser implements CommandParser{

	@Override
	public Project parser(Project project, Element element) {
		if (element.getName().equals("Actor")) {
			Actor actor = new Actor();
			actor.setId(element.getAttributeValue("xmi.id"));
			actor.setName(element.getAttributeValue("name"));
		
			project.getActorList().add(actor);			
		}
			
		return project;
	}

}
