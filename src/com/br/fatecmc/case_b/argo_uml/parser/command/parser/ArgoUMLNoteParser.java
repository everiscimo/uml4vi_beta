package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Note;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.domain.UMLElement;
import com.br.fatecmc.case_b.interfaces.Project;
//import com.br.fatecmc.case_b.domain.Relationship;
//import com.br.fatecmc.case_b.domain.RelationshipType;

public class ArgoUMLNoteParser implements CommandParser {

	@Override
	public Project parser(Project project, Element element) {
		if(element.getName().equals("Comment")){
			Note note = new Note();
			note.setId(element.getAttributeValue("xmi.id"));
			note.setBody(element.getAttributeValue("body"));
			
			for(Element Comment : element.getChildren()){
				for(Element association : Comment.getChildren()){
					
					
					Relationship relationship = new Relationship();
					relationship.setId(Comment.getAttributeValue("xmi.idref"));
					relationship.setRelationshipType(RelationshipType.Comment);
					relationship.getRelationshipComponentList().add(note);
					UMLElement UMLelement = project.searchAll(association
							.getAttributeValue("xmi.idref"));
					
					if(UMLelement instanceof Relationship){
						for(RelationshipComponent relationshipComponent : ((Relationship) UMLelement).getRelationshipComponentList()){
							relationship.getRelationshipComponentList().add(relationshipComponent);	
						}
					}
					else{
						relationship.getRelationshipComponentList().add((RelationshipComponent) UMLelement);	
					}
					note.getRelationshipList().add(relationship);
					break;
				}
					
				
			}
				
			
			project.getNoteList().add(note);
		}
		return project;
	}

}
