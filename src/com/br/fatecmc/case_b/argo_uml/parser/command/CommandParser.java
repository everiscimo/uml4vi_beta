package com.br.fatecmc.case_b.argo_uml.parser.command;

import org.jdom2.Element;

import com.br.fatecmc.case_b.interfaces.Project;

public interface CommandParser {

	public Project parser(Project project,Element element );
	
}
