package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLExtendParser implements CommandParser {

	@Override
	public Project parser(Project project, Element element) {
		
		if (element.getName().equals("Extend")) {
			Relationship relationship = new Relationship();
			relationship.setId(element.getAttributeValue("xmi.id"));
			relationship.setRelationshipType(RelationshipType.Extend);
			
			RelationshipComponent componentAddition = null;
			RelationshipComponent componentBase = null;
			
			for (Element filhosExtend : element.getChildren()) {
				if (filhosExtend.getName().equals("Extend.extension")) {
					for (Element participante : filhosExtend.getChildren()) {
						componentAddition = project.searchUseCase(participante
								.getAttributeValue("xmi.idref"));
					}
				}
				if (filhosExtend.getName().equals("Extend.base")) {
					for (Element participante : filhosExtend.getChildren()) {
						componentBase = project.searchUseCase(participante
								.getAttributeValue("xmi.idref"));
					}
				}

			}
			
			relationship.getRelationshipComponentList().add(componentBase);
			relationship.getRelationshipComponentList().add(componentAddition);

			componentBase.getRelationshipList().add(relationship);
			componentAddition.getRelationshipList().add(relationship);

			
		}

		return project;
	}

}
