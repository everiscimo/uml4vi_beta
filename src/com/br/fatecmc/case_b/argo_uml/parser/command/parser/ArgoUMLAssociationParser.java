package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLAssociationParser implements CommandParser{

	@Override 
	public Project parser(Project project, Element element) {
		if (element.getName().equals("Association")) {
			
			Relationship relationship = new Relationship();
			relationship.setRelationshipType(RelationshipType.Association);
			relationship.setId(element.getAttributeValue("xmi.id"));
			
			Element associacao = null;
			for (Element elementChildren : element.getChildren()) {
				
				if (elementChildren.getName().equals("Association.connection")) {
					associacao = elementChildren;
					break;
				}
			}
			List<RelationshipComponent> componentList = new ArrayList<RelationshipComponent>();
			for (Element AssociationEnd : associacao.getChildren()) {

					if (AssociationEnd.getName().equals("AssociationEnd")) {
						
						for (Element AssociationEndparticipant : AssociationEnd
								.getChildren()) {

							if (AssociationEndparticipant.getName().equals(
									"AssociationEnd.participant")) {
								Element participante = null;
								for (Element procurandoParticipante : AssociationEndparticipant
										.getChildren()) {
									participante = procurandoParticipante;
									break;
								}
								
								
								RelationshipComponent component = project.searchComponent(participante
										.getAttributeValue("xmi.idref"));
								if(component == null){
									System.err.println("Erro ao carregar uma associação (linha 50 Classe ArgoUMLAssociationParser)");
									return null;
								}
										
							
								componentList.add(component);
							}

						}
					}
				
					
			}
			//Criando a associação
			for(RelationshipComponent component : componentList){
				relationship.getRelationshipComponentList().add(component);
			}
			//setando a associação no objeto que a possui
			for(RelationshipComponent component : componentList){
				component.getRelationshipList().add(relationship);
			}
		}

		return project;
	}

}
