package com.br.fatecmc.case_b.argo_uml.parser.command.parser;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.RelationshipType;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLGeneralizationParser implements CommandParser{

	@Override
	public Project parser(Project project, Element element) {
				
		if (element.getName().equals("Generalization")) {
			Relationship relationship = new Relationship();
			relationship.setId(element.getAttributeValue("xmi.id"));
			relationship.setRelationshipType(RelationshipType.Generalization);			

			RelationshipComponent componentAddition = null;
			RelationshipComponent componentBase = null;
			
			for (Element filhosGeneralizacao : element.getChildren()) {
				if (filhosGeneralizacao.getName().equals(
						"Generalization.parent")) {
					for (Element participante : filhosGeneralizacao.
							getChildren()) {
						componentAddition = project.searchComponent(participante.
								getAttributeValue("xmi.idref"));
					}
				}
				if (filhosGeneralizacao.getName().equals(
						"Generalization.child")) {
					for (Element participante : filhosGeneralizacao
							.getChildren()) {
						componentBase = project.searchComponent(participante
								.getAttributeValue("xmi.idref"));
					}
				}

			}
			// 0 - child = componente base
			// 1 - parent = componente addition
			relationship.getRelationshipComponentList().add(componentBase);
			relationship.getRelationshipComponentList().add(componentAddition);
			
			componentBase.getRelationshipList().add(relationship);
			componentAddition.getRelationshipList().add(relationship);
			
		}

		return project;
	}
}
