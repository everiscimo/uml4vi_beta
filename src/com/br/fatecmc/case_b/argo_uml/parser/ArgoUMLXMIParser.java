package com.br.fatecmc.case_b.argo_uml.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import com.br.fatecmc.case_b.argo_uml.parser.command.CommandParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLActorParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLAssociationParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLCommentParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLExtendParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLGeneralizationParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLIncludeParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLNoteParser;
import com.br.fatecmc.case_b.argo_uml.parser.command.parser.ArgoUMLUseCaseParser;
import com.br.fatecmc.case_b.impl.ProjectImpl;
import com.br.fatecmc.case_b.impl.XMIParserImpl;
import com.br.fatecmc.case_b.interfaces.Project;
/**
 * Classe : ArgoUMLXMIParser
 * Descrição:Classe responsavel por interpretar um arquivo
 *			 exportado pelo ArgoUML para um padrão conhecido pelo UML4VI 
 * @author Erico Veriscimo
 * @since feb 2013
 */
public class ArgoUMLXMIParser extends XMIParserImpl{
	private static Element elementSearch;
	private List<CommandParser> parserElementList;
	private List<CommandParser> parserRelationshipList;
	
	/**
	 * Construtor padrão
	 * Descrição: Carrega as duas lista de command a dos elementos e a dos relacionamentos
	 * 
	 */
	public ArgoUMLXMIParser(){
		
		parserRelationshipList = new ArrayList<CommandParser>();
		parserElementList = new ArrayList<CommandParser>();
	
		parserElementList.add(new ArgoUMLActorParser());
		parserElementList.add(new ArgoUMLUseCaseParser());
		parserElementList.add(new ArgoUMLNoteParser());
		parserRelationshipList.add(new ArgoUMLAssociationParser());
		parserRelationshipList.add(new ArgoUMLExtendParser());
		parserRelationshipList.add(new ArgoUMLIncludeParser());
		parserRelationshipList.add(new ArgoUMLGeneralizationParser());
		parserRelationshipList.add(new ArgoUMLCommentParser());
		
		
		
	}
	
	/**
	 * Descrição: é por este método que se sabe 
	 *			se é possivel fazer um parse pela classe,
	 *			em outras palavras este método tem a utilidade
	 *			de verificar se o arquivo in passado por parametro
	 *			é um arquivo exportado pela farramenta ArgoUml
	 * @param InputStream in - um arquivo de entrada
	 * @return Boolean 	true é possivel fazer o parse
	 *					false não é possivel fazer o parse
	 */
	@Override
	public boolean canParse(InputStream in) {
		//setar elementSearch  com null, pois ele é static
		elementSearch = null;
		
		SAXBuilder builder = new SAXBuilder();
		Document document = null;
		try {
			document = builder.build(in);			
		} catch (Exception e) {
			e.getStackTrace();
			return false;
		}
		
		if(document==null){
			return false;
		}
		//procurando pela tag do arquivo xmi "XMI.exporter"
		//caso seja enontrado sera setado na variavel elementSearch
		try {
			elementSearch(document.getRootElement(), "XMI.exporter");
		} catch (Exception e) {
			
		}
		//pegando o valor da tag XMI.exporter onde está o nome da ferramenta que exporto este arquivo
		String exporterString = elementSearch.getValue();
		//pegando o tamanho do texto
		int sizeExporterString = exporterString.length();
		// verificando se existe o nome argoUml na string exporterString caso sim = true, nao = false
		return exporterString.toUpperCase().replace("ARGOUML", "").length()!=sizeExporterString;
	}

	@Override
	public Project parse(InputStream in) throws Exception {
		elementSearch = null;
		Project project = new ProjectImpl();
		SAXBuilder builder = new SAXBuilder();
		Document document = null;
		try {
			document = builder.build(in);			
		} catch (Exception e) {
			e.getStackTrace();
			System.out.println(e.getMessage() +"\n" + e.getStackTrace());
			return null;
		}
		
		if(document==null){
			Exception erro = new Exception("Erro ao carregar arquivo");
			throw erro;
		}
		// Elemento raiz do arquivo xmi carregado
		try {
			elementSearch(document.getRootElement(), "Namespace.ownedElement");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
			
		
		
		if(elementSearch==null || elementSearch.equals(document.getRootElement())){
			Exception erro = new Exception("Erro ao carregar arquivo");
			throw erro;
		}
		for(Element element : elementSearch.getChildren()){
			for(CommandParser parser : parserElementList){
				project = parser.parser(project, element);
			}
		}
		for(Element element : elementSearch.getChildren()){
			for(CommandParser parser : parserRelationshipList){
				project = parser.parser(project, element);
			}
		}
		return project;
	}
	
	/**
	 * Descrição: Metodo para procurar um tag expecifica
	 * 				e setar esta tag em uma variavel estatica elementSearch
	 * @param raiz Element - tag Raiz
	 * @param tag	String - com o nome da tag que procura
	 * @throws Exception
	 */
	public static void elementSearch(Element raiz, String tag)	throws Exception {
		
		if (raiz == null) {
			Exception erro = new Exception("Raiz Nula");
			throw erro;

		}
		if (raiz.getName().equals(tag)) {
			elementSearch = raiz;
			Exception done = new Exception("Tag encontrada");

			throw done;
		}
		for (Element e : raiz.getChildren()) {
			elementSearch(e, tag);
		}

	}
}
