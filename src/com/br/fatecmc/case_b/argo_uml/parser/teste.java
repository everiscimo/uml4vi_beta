package com.br.fatecmc.case_b.argo_uml.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;


import com.br.fatecmc.case_b.domain.Actor;
import com.br.fatecmc.case_b.domain.Note;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.UseCase;
import com.br.fatecmc.case_b.impl.XMIParserImpl;
import com.br.fatecmc.case_b.interfaces.Project;

public class teste {
	static Set<String> primaryKey;
	public static void main(String[] args) throws Exception {
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		primaryKey = new HashSet<String>();

		System.out.println("Arquivo: ");
		String arq = entrada.readLine();
		
		File in = new File(arq);
		//File in = new File("comentario.xmi");
		
		Project proj = null;
		XMIParserImpl pars = new ArgoUMLXMIParser();
		proj = pars.doChain(in);
		System.out.println("Actor:");
		for(Actor a : proj.getActorList()){
			System.out.println(a.getName());
		}
		System.out.println("UseCase:");
		for(UseCase a : proj.getUseCaseList()){
			System.out.println(a.getName());
		}
		System.out.println("Notes:");
		for(Note a : proj.getNoteList()){
			System.out.println(a.getName());
		}
		System.out.println("Relationship:");
		for(Relationship a : proj.getRelationshipList()){
			if(verifyIntegridyKey(a.getId())){
				System.out.println("Type: "+a.getRelationshipType().toString());
				for(RelationshipComponent component : a.getRelationshipComponentList()){
					System.out.println(component.getClass().getSimpleName() +": "+component.getName());
				}
				System.out.println("\n");
			}
			
		}
		
	}
	
	
	
		protected static boolean verifyIntegridyKey(String id){
		Integer size = primaryKey.size();
		primaryKey.add(id);
		return primaryKey.size()!=size; 
	}
	
	
	
}
