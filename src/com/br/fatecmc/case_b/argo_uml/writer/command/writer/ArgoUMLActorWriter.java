package com.br.fatecmc.case_b.argo_uml.writer.command.writer;


import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.command.CommandWriter;
import com.br.fatecmc.case_b.domain.Actor;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLActorWriter implements CommandWriter {


	@Override
	public Element writer(Project project, Element element) {
		for (Actor actor : project.getActorList()) {
			Element Actor = new Element("Actor");
			Actor.setNamespace(uml);
			Actor.setAttribute("xmi.id", actor.getId());
			Actor.setAttribute("name", actor.getName());
			Actor.setAttribute("isSpecification",
					String.valueOf(Boolean.FALSE));
			Actor.setAttribute("isRoot", String.valueOf(Boolean.FALSE));
			Actor.setAttribute("isLeaf", String.valueOf(Boolean.FALSE));
			Actor.setAttribute("isAbstract", String.valueOf(Boolean.FALSE));
			element.addContent(Actor);
		}
		
		return element;
	}



}
