package com.br.fatecmc.case_b.argo_uml.writer.command;


import org.jdom2.Element;
import org.jdom2.Namespace;


import com.br.fatecmc.case_b.interfaces.Project;

public interface  CommandWriter {
	public final static Namespace uml = Namespace.getNamespace("UML",
			"org.omg.xmi.namespace.UML");
	public Element writer(Project project,Element element );
	
}
