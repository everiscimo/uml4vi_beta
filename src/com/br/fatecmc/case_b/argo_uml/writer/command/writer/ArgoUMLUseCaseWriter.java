package com.br.fatecmc.case_b.argo_uml.writer.command.writer;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.command.CommandWriter;
import com.br.fatecmc.case_b.domain.UseCase;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLUseCaseWriter implements CommandWriter {

	@Override
	public Element writer(Project project, Element element) {
		for (UseCase useCase : project.getUseCaseList()) {
			Element Caso = new Element("UseCase");
			Caso.setNamespace(uml);
			Caso.setAttribute("xmi.id", useCase.getId());
			Caso.setAttribute("name", useCase.getName());
			Caso.setAttribute("isSpecification",
					String.valueOf(Boolean.FALSE));
			Caso.setAttribute("isRoot", String.valueOf(Boolean.FALSE));
			Caso.setAttribute("isLeaf", String.valueOf(Boolean.FALSE));
			Caso.setAttribute("isAbstract", String.valueOf(Boolean.FALSE));
			element.addContent(Caso);
		}
		
		return element;
	}

}
