package com.br.fatecmc.case_b.argo_uml.writer.command.writer;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.chain.relationship.ArgoUMLRelationshipAssociationWriter;
import com.br.fatecmc.case_b.argo_uml.writer.chain.relationship.ArgoUMLRelationshipExtendWriter;
import com.br.fatecmc.case_b.argo_uml.writer.chain.relationship.ArgoUMLRelationshipGeneralizationWriter;
import com.br.fatecmc.case_b.argo_uml.writer.chain.relationship.ArgoUMLRelationshipIncludeWriter;
import com.br.fatecmc.case_b.argo_uml.writer.command.CommandWriter;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.interfaces.Project;

public class ArgoUMLRelationshipWriter implements CommandWriter{

	private ArgoUMLRelationshipAssociationWriter association;
	private ArgoUMLRelationshipExtendWriter extend;
	private ArgoUMLRelationshipIncludeWriter include;
	private ArgoUMLRelationshipGeneralizationWriter generalization;
	
	public ArgoUMLRelationshipWriter(){
		
		association = new ArgoUMLRelationshipAssociationWriter();
		extend = new ArgoUMLRelationshipExtendWriter();
		include = new ArgoUMLRelationshipIncludeWriter();
		generalization = new ArgoUMLRelationshipGeneralizationWriter();
		
		association.setSuccessor(extend);
		extend.setSuccessor(include);
		include.setSuccessor(generalization);
		
	}
	

	@Override
	public Element writer(Project project, Element element) {
		for(Relationship relationship : project.getRelationshipList()){
			element = association.writer(relationship, element);
		}
		
		return element;
	}

}
