package com.br.fatecmc.case_b.argo_uml.writer;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

import com.br.fatecmc.case_b.argo_uml.writer.command.CommandWriter;
import com.br.fatecmc.case_b.argo_uml.writer.command.writer.ArgoUMLActorWriter;
import com.br.fatecmc.case_b.argo_uml.writer.command.writer.ArgoUMLRelationshipWriter;
import com.br.fatecmc.case_b.argo_uml.writer.command.writer.ArgoUMLUseCaseWriter;
import com.br.fatecmc.case_b.interfaces.Project;
import com.br.fatecmc.case_b.interfaces.XMIWriter;

public class ArgoUMLXMIWriter implements XMIWriter{

	private List<CommandWriter> writerList;
	
	public ArgoUMLXMIWriter(){
		
		writerList = new ArrayList<CommandWriter>();
		writerList.add(new ArgoUMLActorWriter());
		writerList.add(new ArgoUMLRelationshipWriter());
		writerList.add(new ArgoUMLUseCaseWriter());
	
	}
	
	/**
	 * @param out is a OutputStream.
	 * @param project is a Project.
	 * 
	 * @return  1 - success
	 * 			2 - invalid
	 */
	@Override
	public Long writeProjectTo(OutputStream out, Project project) {
		
		Element content = new Element("XMI.content");
		Element model = new Element("Model");
		Element root = new Element("XMI");
		Element header = new Element("XMI.header");
		
		Element element = headerWriter(project, content, model, root,header);
		
		for(CommandWriter writer : writerList){
			writer.writer(project, element);
		}
		
		model.addContent(element);
		content.addContent(model);
		root.addContent(header);
		root.addContent(content);
		
		Document doc = new Document();
		doc.setRootElement(root);

	
        XMLOutputter xout = new XMLOutputter();
        try {
        	  xout.output(doc, out);
              
		} catch (Exception e) {
			e.getStackTrace();
			return 2l;
		}
      
		return 1l;
	}
	
	public ArgoUMLXMIWriter clone() {
        Object clone = null;
        try {
                clone = super.clone();
        } catch (CloneNotSupportedException ex) {
                ex.printStackTrace();
        }
        return (ArgoUMLXMIWriter) clone;
	 }

	public Element headerWriter(Project project, Element content, Element model, Element root, Element header){
		
		root.setAttribute("xmi.version", project.getVersion());
		root.addNamespaceDeclaration(CommandWriter.uml);
		root.setAttribute("timestamp", new Date().toString());

		
		Element documentation = new Element("XMI.documentation");
		Element exporter = new Element("XMI.exporter");
		exporter.setText("UML4VI-ArgoUML");
		Element exporterVersion = new Element("XMI.exporterVersion");
		exporterVersion.setText("1.2");
		documentation.addContent(exporter);
		documentation.addContent(exporterVersion);
		header.addContent(documentation);
		Element metamodel = new Element("XMI.metamodel");
		metamodel.setAttribute("xmi.name", "UML");
		metamodel.setAttribute("xmi.version", "1.4");
		header.addContent(metamodel);

		
		model.setNamespace(CommandWriter.uml);
		model.setAttribute("xmi.id", "UML4VIid");
		model.setAttribute("name", project.getName());
		model.setAttribute("isSpecification", Boolean.FALSE.toString());
		model.setAttribute("isRoot", Boolean.FALSE.toString());
		model.setAttribute("isLeaf", Boolean.FALSE.toString());
		model.setAttribute("isAbstract", Boolean.FALSE.toString());
		Element ownedElement = new Element("Namespace.ownedElement");
		ownedElement.setNamespace(CommandWriter.uml);
		
		return ownedElement;
		
	}
}
