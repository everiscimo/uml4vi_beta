package com.br.fatecmc.case_b.argo_uml.writer.chain.relationship;


import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.chain.AbstractRelationshipChain;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;


public class ArgoUMLRelationshipIncludeWriter extends AbstractRelationshipChain{

	
	
	
	@Override
	public boolean canWriter(RelationshipType relationshipType) {
		// TODO Auto-generated method stub
		return relationshipType == RelationshipType.Include;
	}

	
	/**
	 * Element 0 getRelationshipList Addition
	 * Element 1 getRelationshipList Base
	 */
	@Override
	public Element writer(Relationship include, Element element) {
		if(canWriter(include.getRelationshipType()) && verifyIntegridyKey(include.getId())){
			Element Include = new Element("Include");
			Include.setNamespace(uml);
			Include.setAttribute("xmi.id", String.valueOf(include.getId()));
			Include.setAttribute("isSpecification", "false");

			// Elemento addition
			Element addition = new Element("Include.addition");
			addition.setNamespace(uml);
			Element UseCaseAddition = new Element("UseCase");
			UseCaseAddition.setNamespace(uml);
			UseCaseAddition.setAttribute("xmi.idref", include.getRelationshipComponentList().get(0).getId());
			addition.addContent(UseCaseAddition);
			Include.addContent(addition);

			// Elemento Base
			Element base = new Element("Include.base");
			base.setNamespace(uml);
			Element UseCaseBase = new Element("UseCase");
			UseCaseBase.setNamespace(uml);
			UseCaseBase.setAttribute("xmi.idref",include.getRelationshipComponentList().get(1).getId());
			base.addContent(UseCaseBase);
			Include.addContent(base);

			element.addContent(Include);

		}
		else{
			if(getSuccessor()==null){
				return element;
			}
			return ((AbstractRelationshipChain) getSuccessor()).writer(include, element);
		}
		
		return element;
	}

}
