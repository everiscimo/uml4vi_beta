package com.br.fatecmc.case_b.argo_uml.writer.chain.relationship;


import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.chain.AbstractRelationshipChain;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;

public class ArgoUMLRelationshipAssociationWriter extends AbstractRelationshipChain{

	
	@Override
	public boolean canWriter(RelationshipType relationshipType) {
		// TODO Auto-generated method stub
		return relationshipType == RelationshipType.Association;
	}

	/**
	 * element 0 getRelationshipList actor
	 * element 1 getRelationshipList usecase
	 */
	@Override
	public Element writer(Relationship association, Element element) {
		if(canWriter(association.getRelationshipType()) && verifyIntegridyKey(association.getId())){
			Element Association = new Element("Association");
			Association.setNamespace(uml);
			Association.setAttribute("xmi.id", String.valueOf(association.getId()));
			Association.setAttribute("name", "");
			Association.setAttribute("isSpecification", "false");
			Association.setAttribute("isRoot", "false");
			Association.setAttribute("isLeaf", "false");
			Association.setAttribute("isAbstract", "false");
			Element AssociationConnection = new Element(
					"Association.connection");
			AssociationConnection.setNamespace(uml);
			Association.addContent(AssociationConnection);
			// element 1
			Element AssociationEnd = new Element("AssociationEnd");
			AssociationEnd.setNamespace(uml);
			AssociationEnd.setAttribute("xmi.id", String.valueOf(association.getId()+"a"));
			AssociationConnection.addContent(AssociationEnd);

			Element AssociationEndparticipant = new Element(
					"AssociationEnd.participant");
			AssociationEndparticipant.setNamespace(uml);
			AssociationEnd.addContent(AssociationEndparticipant);

			Element ActorAssociacao = new Element("Actor");
			ActorAssociacao.setNamespace(uml);
			ActorAssociacao.setAttribute("xmi.idref", association.getRelationshipComponentList().get(0).getId());
			AssociationEndparticipant.addContent(ActorAssociacao);

			// element 2
			Element AssociationEnd2 = new Element("AssociationEnd");
			AssociationEnd2.setNamespace(uml);
			AssociationEnd2.setAttribute("xmi.id", String.valueOf(association.getId()+"b"));
			AssociationConnection.addContent(AssociationEnd2);

			Element AssociationEndparticipant2 = new Element(
					"AssociationEnd.participant");
			AssociationEndparticipant2.setNamespace(uml);
			AssociationEnd2.addContent(AssociationEndparticipant2);

			Element CasoAssociacao = new Element("Case");
			CasoAssociacao.setNamespace(uml);
			CasoAssociacao.setAttribute("xmi.idref",association.getRelationshipComponentList().get(1).getId());
			AssociationEndparticipant2.addContent(CasoAssociacao);

			element.addContent(Association);
		}
		else{
			if(getSuccessor()==null){
				return element;
			}
			return ((AbstractRelationshipChain) getSuccessor()).writer(association, element);
		}
		
		return element;
	}

	
}
