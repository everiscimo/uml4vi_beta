package com.br.fatecmc.case_b.argo_uml.writer.chain.relationship;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.chain.AbstractRelationshipChain;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;


public class ArgoUMLRelationshipExtendWriter extends AbstractRelationshipChain {

	@Override
	public boolean canWriter(RelationshipType relationshipType) {

		return relationshipType == RelationshipType.Extend;

	}

	/**
	 * element 0 getRelationshipList Base 
	 * element 1 getRelationshipList Extension
	 */
	@Override
	public Element writer(Relationship extend, Element element) {

		if(canWriter(extend.getRelationshipType()) && verifyIntegridyKey(extend.getId())){
			
			Element Extend = new Element("Extend");
			Extend.setNamespace(uml);
			Extend.setAttribute("xmi.id", String.valueOf(extend.getId()));
			Extend.setAttribute("isSpecification", "false");
	
			// ELemento Base
			Element base = new Element("Extend.base");
			base.setNamespace(uml);
			Element UseCaseBase = new Element("UseCase");
			UseCaseBase.setNamespace(uml);
			UseCaseBase.setAttribute("xmi.idref",
					extend.getRelationshipComponentList().get(0).getId());
			base.addContent(UseCaseBase);
			Extend.addContent(base);
	
			// ELemento extension
			Element extension = new Element("Extend.extension");
			extension.setNamespace(uml);
			Element UseCaseExtension = new Element("UseCase");
			UseCaseExtension.setNamespace(uml);
			UseCaseExtension.setAttribute("xmi.idref", extend.getRelationshipComponentList()
					.get(1).getId());
			extension.addContent(UseCaseExtension);
			Extend.addContent(extension);
	
			element.addContent(Extend);
		}
		else{
			if(getSuccessor()==null){
				return element;
			}
			return ((AbstractRelationshipChain) getSuccessor()).writer(extend, element);
		}
		
		return element;
	}

}
