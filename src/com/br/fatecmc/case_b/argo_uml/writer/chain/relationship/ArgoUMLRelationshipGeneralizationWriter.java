package com.br.fatecmc.case_b.argo_uml.writer.chain.relationship;

import org.jdom2.Element;

import com.br.fatecmc.case_b.argo_uml.writer.chain.AbstractRelationshipChain;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;

public class ArgoUMLRelationshipGeneralizationWriter extends AbstractRelationshipChain{

	@Override
	public boolean canWriter(RelationshipType relationshipType) {
		// TODO Auto-generated method stub
		return relationshipType == RelationshipType.Generalization;
	}

	
	/**
	 * Element 0 getRelationshipList child
	 * Element 1 getRelationshipList parent 
	 */
	@Override
	public Element writer(Relationship generalization, Element element) {
		if(canWriter(generalization.getRelationshipType()) && verifyIntegridyKey(generalization.getId()) ){
			Element Generalization = new Element("Generalization");
			Generalization.setNamespace(uml);
			Generalization.setAttribute("xmi.id", String.valueOf(generalization.getId()));
			Generalization.setAttribute("isSpecification", "false");

			// elemento Child
			Element child = new Element("Generalization.child");
			child.setNamespace(uml);
			Element ActorChild = new Element("Actor");
			ActorChild.setNamespace(uml);
			ActorChild.setAttribute("xmi.idref", generalization.getRelationshipComponentList().get(0).getId());
			child.addContent(ActorChild);
			Generalization.addContent(child);

			// elemento parent
			Element parent = new Element("Generalization.parent");
			parent.setNamespace(uml);
			Element ActorParent = new Element("Actor");
			ActorParent.setNamespace(uml);
			ActorParent.setAttribute("xmi.idref", generalization.getRelationshipComponentList().get(1).getId());
			parent.addContent(ActorParent);
			Generalization.addContent(parent);

			element.addContent(Generalization);
		}
		else{
			if(getSuccessor()==null){
				return element;
			}
			return ((AbstractRelationshipChain) getSuccessor()).writer(generalization, element);
		}
		
		return element;
	}

}
