package com.br.fatecmc.case_b.argo_uml.writer.chain;


import org.jdom2.Namespace;


public abstract class AbstractChain {
	protected Namespace uml = Namespace.getNamespace("UML",
			"org.omg.xmi.namespace.UML");
	private AbstractChain successor;
	
	public void setSuccessor(AbstractChain successor) {
		this.successor = successor;
	}
	public AbstractChain getSuccessor() {
		return successor;
	}

	public AbstractChain(){
		successor=null;
	}
}
