package com.br.fatecmc.case_b.argo_uml.writer.chain;


import java.util.HashSet;
import java.util.Set;

import org.jdom2.Element;

import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipType;


public abstract class AbstractRelationshipChain extends AbstractChain  {

	private Set<String> primaryKey;
	
	public AbstractRelationshipChain(){
		primaryKey = new HashSet<String>();
	}
	
	public abstract boolean canWriter(RelationshipType relationshipType);
	public abstract Element writer(Relationship relationship, Element element);
	

	protected boolean verifyIntegridyKey(String id){
		Integer size = primaryKey.size();
		primaryKey.add(id);
		return primaryKey.size()!=size; 
	}
	
}
