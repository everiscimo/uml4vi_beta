package com.br.fatecmc.case_b.interfaces;

public interface XMIWriterFactory {

	public XMIWriter create(String toolName, String toolVersion);
	
}
