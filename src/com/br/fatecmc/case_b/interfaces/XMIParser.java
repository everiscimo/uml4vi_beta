package com.br.fatecmc.case_b.interfaces;


import java.io.File;
import java.io.InputStream;


public interface XMIParser {

	public boolean canParse(InputStream in);
	public Project parse(InputStream in)throws Exception;
	public Project doChain(File in);
	
}
