package com.br.fatecmc.case_b.interfaces;

import java.io.OutputStream;


public interface XMIWriter extends Cloneable {

	public Long writeProjectTo(OutputStream out, Project project);
	
	public XMIWriter clone();
}
