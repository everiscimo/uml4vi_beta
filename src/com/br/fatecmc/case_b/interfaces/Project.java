package com.br.fatecmc.case_b.interfaces;


import java.util.List;

import com.br.fatecmc.case_b.domain.Actor;
import com.br.fatecmc.case_b.domain.Note;
import com.br.fatecmc.case_b.domain.Relationship;
import com.br.fatecmc.case_b.domain.RelationshipComponent;
import com.br.fatecmc.case_b.domain.UMLElement;
import com.br.fatecmc.case_b.domain.UseCase;
/**
 * Interface for project.
 * @author Erico Veriscimo
 *
 */
public interface Project {

	public List<Actor> getActorList();
	public List<UseCase> getUseCaseList();
	public List<Note> getNoteList();
	public List<Relationship> getRelationshipList();
	public Actor searchActor(String id);
	public UseCase searchUseCase(String id);
	public RelationshipComponent searchComponent(String id);
	public Relationship searchRelationship(String id);
	public Note searchNote(String id);
	public String getName();
	public String getVersion();
	public UMLElement searchAll(String id);
	public List<com.br.fatecmc.case_b.domain.Class> getClassList();
}
