package com.br.fatecmc.case_b.domain;

import java.util.ArrayList;
import java.util.List;

public class Method {
	
	private String id;
	private String name;
	private AttributeType returnMethod;
	private List<Attribute> paramenterList;
	private AccessModifiers accessModifiers;
	private List<Modifiers> modifiersList;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the returnMethod
	 */
	public AttributeType getReturnMethod() {
		return returnMethod;
	}
	/**
	 * @param returnMethod the returnMethod to set
	 */
	public void setReturnMethod(AttributeType returnMethod) {
		this.returnMethod = returnMethod;
	}
	/**
	 * @return the accessModifiers
	 */
	public AccessModifiers getAccessModifiers() {
		return accessModifiers;
	}
	/**
	 * @param accessModifiers the accessModifiers to set
	 */
	public void setAccessModifiers(AccessModifiers accessModifiers) {
		this.accessModifiers = accessModifiers;
	}
	/**
	 * @return the modifiersList
	 */
	public List<Modifiers> getModifiersList() {
		return modifiersList;
	}
	/**
	 * @param modifiersList the modifiersList to set
	 */
	public void setModifiersList(List<Modifiers> modifiersList) {
		this.modifiersList = modifiersList;
	}
	/**
	 * @return the paramenterList
	 */
	public List<Attribute> getParamenterList() {
		if(paramenterList == null){
			paramenterList = new ArrayList<Attribute>();
		}
		return paramenterList;
	}
	
	
	
}
