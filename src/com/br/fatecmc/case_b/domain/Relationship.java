package com.br.fatecmc.case_b.domain;

import java.util.ArrayList;
import java.util.List;

public class Relationship implements UMLElement {

	private String id;
	private RelationshipType relationshipType;
	private List<RelationshipComponent> relationshipList;
		
	/**
	 * @return the relationshipList
	 */
	
	public List<RelationshipComponent> getRelationshipComponentList() {
		if(relationshipList==null){
			relationshipList = new ArrayList<RelationshipComponent>(); 
		}
		return relationshipList;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the relationshipType
	 */
	public RelationshipType getRelationshipType() {
		return relationshipType;
	}
	/**
	 * @param relationshipType the relationshipType to set
	 */
	public void setRelationshipType(RelationshipType relationshipType) {
		this.relationshipType = relationshipType;
	}
	
	
}
