package com.br.fatecmc.case_b.domain;

import java.util.ArrayList;
import java.util.List;

public class Class implements RelationshipComponent{

	private String id;
	private String name;
	private List<Relationship> relationlist;
	private AccessModifiers accessModifiers;
	private List<Modifiers> modifiersList;
	private List<Method> methodList;
	private List<Attribute> attributeList;
	
	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public List<Relationship> getRelationshipList() {
		if(relationlist == null){
			relationlist = new ArrayList<Relationship>();
		}
		return relationlist;
	}

	/**
	 * @return the relationlist
	 */
	public List<Relationship> getRelationlist() {
		return relationlist;
	}

	/**
	 * @param relationlist the relationlist to set
	 */
	public void setRelationlist(List<Relationship> relationlist) {
		this.relationlist = relationlist;
	}

	/**
	 * @return the accessModifiers
	 */
	public AccessModifiers getAccessModifiers() {
		return accessModifiers;
	}

	/**
	 * @param accessModifiers the accessModifiers to set
	 */
	public void setAccessModifiers(AccessModifiers accessModifiers) {
		this.accessModifiers = accessModifiers;
	}

	/**
	 * @return the modifiersList
	 */
	public List<Modifiers> getModifiersList() {
		return modifiersList;
	}

	/**
	 * @param modifiersList the modifiersList to set
	 */
	public void setModifiersList(List<Modifiers> modifiersList) {
		this.modifiersList = modifiersList;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the methodList
	 */
	public List<Method> getMethodList() {
		if(methodList == null){
			methodList = new ArrayList<Method>();
		}
		return methodList;
	}

	/**
	 * @return the attributeList
	 */
	public List<Attribute> getAttributeList() {
		if(attributeList == null){
			attributeList = new ArrayList<Attribute>();
		}
		return attributeList;
	}
	
	

}
