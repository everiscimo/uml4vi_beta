package com.br.fatecmc.case_b.domain;

public enum RelationshipType {
	Comment(5),
	Extend(4),
	Generalization(3), 
	Include(2), 
	Association(1);
	
	private int value;
	
	RelationshipType(int value){
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	
	
}
