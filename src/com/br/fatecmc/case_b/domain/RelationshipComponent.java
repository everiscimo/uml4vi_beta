package com.br.fatecmc.case_b.domain;


import java.util.List;

public interface RelationshipComponent extends UMLElement{
	
	public String getId();
	public String getName();
	public List<Relationship> getRelationshipList();
}
