package com.br.fatecmc.case_b.domain;

public enum AccessModifiers {

	DEFAULT(1),
	PUBLIC(2),
	PRIVATE(3),
	PROTECTED(4);
	
	private int value;
	
	AccessModifiers(int value){
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
}
