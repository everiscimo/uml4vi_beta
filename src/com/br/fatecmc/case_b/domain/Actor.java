package com.br.fatecmc.case_b.domain;

import java.util.ArrayList;
import java.util.List;


/**
 * Class actor
 * 
 * @author Erico Veriscimo
 * 
 */
public class Actor implements RelationshipComponent {

	private String id;
	private String name;
	private List<Relationship> relationshipList;
	
	/**
	 * @return the relationshipList
	 */
	
	public List<Relationship> getRelationshipList() {
		if(relationshipList==null){
			relationshipList = new ArrayList<Relationship>(); 
		}
		return relationshipList;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	
}
