package com.br.fatecmc.case_b.domain;

import java.util.List;

public class Attribute {

	private String id;
	private String name;
	private AccessModifiers accessModifiers;
	private List<Modifiers> modifiersList;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the accessModifiers
	 */
	public AccessModifiers getAccessModifiers() {
		return accessModifiers;
	}
	/**
	 * @param accessModifiers the accessModifiers to set
	 */
	public void setAccessModifiers(AccessModifiers accessModifiers) {
		this.accessModifiers = accessModifiers;
	}
	/**
	 * @return the modifiersList
	 */
	public List<Modifiers> getModifiersList() {
		return modifiersList;
	}
	/**
	 * @param modifiersList the modifiersList to set
	 */
	public void setModifiersList(List<Modifiers> modifiersList) {
		this.modifiersList = modifiersList;
	}
	/**
	 * @return the attributeType
	 */
	public AttributeType getAttributeType() {
		return attributeType;
	}
	/**
	 * @param attributeType the attributeType to set
	 */
	public void setAttributeType(AttributeType attributeType) {
		this.attributeType = attributeType;
	}
	private AttributeType attributeType;
	
	
	
}
