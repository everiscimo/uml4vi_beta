package com.br.fatecmc.case_b.domain;

import java.util.ArrayList;
import java.util.List;


public class Note implements  RelationshipComponent {
	 

	private String id;
	private String body;
	private List<Relationship> relationshipList;
	
	/**
	 * @return the relationshipList
	 */
	
	public List<Relationship> getRelationshipList() {
		if(relationshipList==null){
			relationshipList = new ArrayList<Relationship>(); 
		}
		return relationshipList;
	}
	


	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	@Override
	public String getName() {
		return body;
	}



	
	

}
