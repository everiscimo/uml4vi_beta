package com.br.fatecmc.case_b.domain;

public enum Modifiers {
	INTERFACE(1),
	STATIC(2),
	ABSTRACT(3),
	FINAL(4);
	
	private int value;
	
	Modifiers(int value){
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	
	
}
